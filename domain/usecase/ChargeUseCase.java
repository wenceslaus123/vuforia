package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.data.model.responce.Charge;
import com.vuforia.vuforia.data.repository.CountriesRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ChargeUseCase extends UseCase<Charge, Void> {

    private final CountriesRepository repository;
    private String artId;
    private String countryId;

    @Inject
    ChargeUseCase(CountriesRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<Charge> buildUseCaseObservable(Void v) {
        return repository.getCharge(artId, countryId);
    }

    public ChargeUseCase withData(String artId, String countryId) {
        this.artId = artId;
        this.countryId = countryId;
        return this;
    }
}
