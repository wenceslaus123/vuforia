package com.vuforia.vuforia.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

@SuppressWarnings("PMD")
public class ArtPiece implements Parcelable {

    private final long id;

    private final String name;

    private final double height;

    private final double width;

    private final int beamHeight;

    private final int beamWidth;

    private final String measuringType;

    private final String placeName;

    private final long price;

    private final String mediumType;

    private final boolean isFramed;

    private final List<String> suitableRooms;

    private final String imageUrl;

    private final String thumbImageUrl;

    private final String artistName;

    private final String artistPhoto;

    private final String bio;

    private final String artistCountry;

    private final String sizeIndicator;

    private final String currency;

    private ArtPiece(Builder builder) {
        id = builder.id;
        name = builder.name;
        height = builder.height;
        width = builder.width;
        beamHeight = builder.beamHeight;
        beamWidth = builder.beamWidth;
        measuringType = builder.measuringType;
        placeName = builder.placeName;
        price = builder.price;
        mediumType = builder.mediumType;
        isFramed = builder.isFramed;
        suitableRooms = builder.suitableRooms;
        imageUrl = builder.imageUrl;
        thumbImageUrl = builder.thumbImageUrl;
        artistName = builder.artistName;
        artistPhoto = builder.artistPhoto;
        bio = builder.bio;
        artistCountry = builder.artistCountry;
        sizeIndicator = builder.sizeIndicator;
        currency = builder.currency;
    }

    public static Builder builder() {
        return new Builder();
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public int getBeamHeight() {
        return beamHeight;
    }

    public int getBeamWidth() {
        return beamWidth;
    }

    public String getMeasuringType() {
        return measuringType;
    }

    public String getPlaceName() {
        return placeName;
    }

    public long getPrice() {
        return price;
    }

    public String getMediumType() {
        return mediumType;
    }

    public boolean isFramed() {
        return isFramed;
    }

    public List<String> getSuitableRooms() {
        return suitableRooms;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getThumbImageUrl() {
        return thumbImageUrl;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getArtistPhoto() {
        return artistPhoto;
    }

    public String getBio() {
        return bio;
    }

    public String getArtistCountry() {
        return artistCountry;
    }

    public String getSizeIndicator() {
        return sizeIndicator;
    }

    public String getCurrency() {
        return currency;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeDouble(this.height);
        dest.writeDouble(this.width);
        dest.writeInt(this.beamHeight);
        dest.writeInt(this.beamWidth);
        dest.writeString(this.measuringType);
        dest.writeString(this.placeName);
        dest.writeLong(this.price);
        dest.writeString(this.mediumType);
        dest.writeByte(this.isFramed ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.suitableRooms);
        dest.writeString(this.imageUrl);
        dest.writeString(this.thumbImageUrl);
        dest.writeString(this.artistName);
        dest.writeString(this.artistPhoto);
        dest.writeString(this.bio);
        dest.writeString(this.artistCountry);
        dest.writeString(this.sizeIndicator);
        dest.writeString(this.currency);
    }




    protected ArtPiece(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.height = in.readDouble();
        this.width = in.readDouble();
        this.beamHeight = in.readInt();
        this.beamWidth = in.readInt();
        this.measuringType = in.readString();
        this.placeName = in.readString();
        this.price = in.readLong();
        this.mediumType = in.readString();
        this.isFramed = in.readByte() != 0;
        this.suitableRooms = in.createStringArrayList();
        this.imageUrl = in.readString();
        this.thumbImageUrl = in.readString();
        this.artistName = in.readString();
        this.artistPhoto = in.readString();
        this.bio = in.readString();
        this.artistCountry = in.readString();
        this.sizeIndicator = in.readString();
        this.currency = in.readString();
    }

    public static final Creator<ArtPiece> CREATOR = new Creator<ArtPiece>() {
        @Override
        public ArtPiece createFromParcel(Parcel source) {
            return new ArtPiece(source);
        }

        @Override
        public ArtPiece[] newArray(int size) {
            return new ArtPiece[size];
        }
    };

    public static final class Builder {
        private long id;
        private String name;
        private double height;
        private double width;
        private int beamHeight;
        private int beamWidth;
        private String measuringType;
        private String placeName;
        private long price;
        private String mediumType;
        private boolean isFramed;
        private List<String> suitableRooms;
        private String imageUrl;
        private String thumbImageUrl;
        private String artistName;
        private String artistPhoto;
        private String bio;
        private String artistCountry;
        private String sizeIndicator;
        private String currency;

        private Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder height(double val) {
            height = val;
            return this;
        }

        public Builder width(double val) {
            width = val;
            return this;
        }

        public Builder beamHeight(int val) {
            beamHeight = val;
            return this;
        }

        public Builder beamWidth(int val) {
            beamWidth = val;
            return this;
        }

        public Builder measuringType(String val) {
            measuringType = val;
            return this;
        }

        public Builder placeName(String val) {
            placeName = val;
            return this;
        }

        public Builder price(long val) {
            price = val;
            return this;
        }

        public Builder mediumType(String val) {
            mediumType = val;
            return this;
        }

        public Builder isFramed(boolean val) {
            isFramed = val;
            return this;
        }

        public Builder suitableRooms(List<String> val) {
            suitableRooms = val;
            return this;
        }

        public Builder imageUrl(String val) {
            imageUrl = val;
            return this;
        }

        public Builder thumbImageUrl(String val) {
            thumbImageUrl = val;
            return this;
        }

        public Builder artistName(String val) {
            artistName = val;
            return this;
        }

        public Builder artistPhoto(String val) {
            artistPhoto = val;
            return this;
        }

        public Builder bio(String val) {
            bio = val;
            return this;
        }

        public Builder artistCountry(String val) {
            artistCountry = val;
            return this;
        }

        public Builder sizeIndicator(String val) {
            sizeIndicator = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public ArtPiece build() {
            return new ArtPiece(this);
        }
    }
}
