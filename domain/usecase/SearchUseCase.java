package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.data.model.responce.ArtpieceSearchResponse;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.domain.repository.PicturesRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

public class SearchUseCase extends UseCase<List<ArtPiece>, String> {

    private final PicturesRepository repository;
    private long totalCount;
    private long pageNumber;
    private long pageSize;
    private long totalPages;
    private String query;

    @Inject
    SearchUseCase(PicturesRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<List<ArtPiece>> buildUseCaseObservable(final String params) {
        return repository.search(params, pageNumber + 1)
                .map(new Function<ArtpieceSearchResponse, List<ArtPiece>>() {
                    @Override
                    public List<ArtPiece> apply(@NonNull ArtpieceSearchResponse artpieceSearchResponse) throws Exception {
                        totalCount = artpieceSearchResponse.totalCount;
                        pageNumber = artpieceSearchResponse.page;
                        pageSize = artpieceSearchResponse.pageSize;
                        totalPages = artpieceSearchResponse.totalPages;
                        return artpieceSearchResponse.convertToArtpiceList();
                    }
                });
    }

    public long getTotalCount() {
        return totalCount;
    }

    public long getPageNumber() {
        return pageNumber;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void search(@android.support.annotation.NonNull DisposableObserver<List<ArtPiece>> observer, String query) {
        this.query = query;
        pageNumber = 0;
        execute(observer, query);
    }

    public void loadMore(@android.support.annotation.NonNull DisposableObserver<List<ArtPiece>> observer) {
        execute(observer, query);
    }
}
