package com.vuforia.vuforia.utils;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateUtils {

    private DateUtils() {
    }

    public static String getDateFromString(String currentDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date parseDate = null;
        try {
            parseDate = format.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (parseDate == null) {
            return "dd/MM/yyyy";
        } else {
            return DateFormat.format("dd/MM/yyyy", parseDate).toString();
        }
    }
}
