package com.vuforia.vuforia.presentation.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.domain.repository.PicturesRepository;
import com.vuforia.vuforia.domain.usecase.SearchUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

public class SearchPresenter extends Presenter<SearchPresenter.SearchView> {

    private final SearchUseCase searchUseCase;
    private ArtPiece selectedArtPiece;
    private int actionType;

    @Inject
    public SearchPresenter(SearchUseCase searchUseCase) {
        this.searchUseCase = searchUseCase;
    }

    public void search(String query) {
        getView().showProgress();
        searchUseCase.search(new SearchResultObserver(), TextUtils.isEmpty(query) ? null : query);
    }

    @Override
    protected void onAttachView(@NonNull SearchView view) {
        //Nothing need here
    }

    @Override
    protected void onDetachView(@NonNull SearchView view) {
        searchUseCase.dispose();
    }

    @Override
    protected void onDestroy() {
        //Nothing need here
    }

    public void setSelectedArtPiece(ArtPiece selectedArtPiece, int actionType) {
        this.selectedArtPiece = selectedArtPiece;
        this.actionType = actionType;
    }

    public ArtPiece getSelectedArtPiece() {
        return selectedArtPiece;
    }

    public int getActionType() {
        return actionType;
    }

    public void loadMore() {
        getView().showProgress();
        searchUseCase.loadMore(new UpdateResultObserver());
    }

    public boolean isLastPage() {
        return searchUseCase.getPageNumber() == searchUseCase.getTotalPages();
    }

    public int getPageSize() {
        return PicturesRepository.PAGE_SIZE;
    }

    public int getTotalCount() {
        return (int) searchUseCase.getTotalCount();
    }

    public interface SearchView extends IView {
        void showResult(List<ArtPiece> data);

        void updateResult(List<ArtPiece> data);
    }

    class SearchResultObserver extends DisposableObserver<List<ArtPiece>> {

        @Override
        public void onNext(@NonNull List<ArtPiece> artPieces) {
            SearchView view = getView();
            if (view != null) {
                view.showResult(artPieces);
                view.hideProgress();
            }
        }

        @Override
        public void onError(@NonNull Throwable e) {
            Timber.e(e);
            SearchView view = getView();
            if (view != null) {
                view.hideProgress();
                if (e instanceof CallServerException) {
                    getView().showError(((CallServerException) e));
                } else {
                    getView().showError(R.string.internal_server_error);
                }
            }
        }

        @Override
        public void onComplete() {
            //Nothing need here
        }
    }

    private class UpdateResultObserver extends SearchResultObserver {
        @Override
        public void onNext(@NonNull List<ArtPiece> artPieces) {
            SearchView view = getView();
            if (view != null) {
                view.updateResult(artPieces);
                view.hideProgress();
            }
        }
    }
}
