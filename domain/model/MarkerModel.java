package com.vuforia.vuforia.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MarkerModel implements Parcelable {

    public static final Creator<MarkerModel> CREATOR = new Creator<MarkerModel>() {
        @Override
        public MarkerModel createFromParcel(Parcel in) {
            return new MarkerModel(in);
        }

        @Override
        public MarkerModel[] newArray(int size) {
            return new MarkerModel[size];
        }
    };
    private final String email;

    protected MarkerModel(Parcel in) {
        email = in.readString();
    }

     MarkerModel(Builder builder) {
        email = builder.email;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "MarkerModel{" +
                "email=" + email +
                '}';
    }


    public static final class Builder {
        private String email;

        public MarkerModel build() {
            return new MarkerModel(this);
        }

        public Builder email(String val) {
            email = val;
            return this;
        }
    }
}
