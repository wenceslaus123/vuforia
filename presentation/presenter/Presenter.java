package com.vuforia.vuforia.presentation.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

public class Presenter<V extends IView> implements IPresenter<V> {

    @Nullable
    private WeakReference<V> mViewWeakReference;

    public final void attachView(@NonNull V view) {
        mViewWeakReference = new WeakReference<>(view);
        onAttachView(view);
    }

    public final void detachView() {
        if (mViewWeakReference != null && mViewWeakReference.get() != null) {
            onDetachView(mViewWeakReference.get());
            mViewWeakReference.clear();
        }
        mViewWeakReference = null;
    }

    public boolean isViewAttached() {
        return mViewWeakReference != null && mViewWeakReference.get() != null;
    }

    @Nullable
    public V getView() {
        if (mViewWeakReference != null && mViewWeakReference.get() != null) {
            return mViewWeakReference.get();
        }
        return null;
    }

    protected void onAttachView(@NonNull V view) {
        //DO NOTHING
    }

    protected void onDetachView(@NonNull V view) {
        //DO NOTHING
    }

    protected void onDestroy() {
        //DO NOTHING
    }
}
