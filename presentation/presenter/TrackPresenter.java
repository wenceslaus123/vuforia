package com.vuforia.vuforia.presentation.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.domain.model.FeedbackModel;
import com.vuforia.vuforia.domain.model.PurchaseDetailModel;
import com.vuforia.vuforia.domain.usecase.FeedbackUseCase;
import com.vuforia.vuforia.domain.usecase.TrackUseCase;
import com.vuforia.vuforia.utils.DateUtils;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class TrackPresenter extends Presenter<TrackPresenter.TrackView> {

    private final TrackUseCase trackUseCase;
    private final FeedbackUseCase feedbackUseCase;
    private long id;

    @Inject
    public TrackPresenter(TrackUseCase trackUseCase, FeedbackUseCase feedbackUseCase) {
        this.trackUseCase = trackUseCase;
        this.feedbackUseCase = feedbackUseCase;

    }

    public void track(String query) {
        if (getView() != null) {
            getView().showProgress();
            trackUseCase.execute(new TrackResultObserver(), TextUtils.isEmpty(query) ? null : query);
        }
    }

    public void postFeedback(boolean isSatisfied, String comment, int experienceBarRating) {
        TrackView view = getView();

        if (view == null) {
            return;
        }

        if (TextUtils.isEmpty(comment)) {
            view.showError(R.string.empty_feedback);
            return;
        } else if (comment.length() < 6 || comment.length() > 600) {
            view.showError(R.string.feedback_wrong_lenght);
            return;
        } else if (experienceBarRating < 1) {
            view.showError(R.string.rate_error);
            return;
        }

        FeedbackModel model = FeedbackModel.builder()
                .sailId((int) id)
                .buyerHappinessLevel(isSatisfied)
                .comment(comment)
                .overallExperience(experienceBarRating)
                .build();
        post(model);

    }

    private void post(FeedbackModel model) {
        if (getView() != null) {
            getView().showProgress();
            feedbackUseCase.execute(new FeedbackResultObserver(), model);
        }
    }

    @Override
    protected void onAttachView(@NonNull TrackPresenter.TrackView view) {
        //Nothing need here
    }

    @Override
    protected void onDetachView(@NonNull TrackPresenter.TrackView view) {
        //searchUseCase.dispose();
    }

    @Override
    protected void onDestroy() {
        //Nothing need here
    }

    public void showResult(PurchaseDetailModel response) {
        {
            String number = response.getTrackingNumber();
            if (getView() != null) {
                getView().switchFeedbackScreen();
                id = response.getId();
                if (number != null) {
                    getView().setTrackingNumber(number);
                }
                String name = response.getNameOfArt();
                if (name != null) {
                    getView().setArtName(name);
                }
                String company = response.getShippingCompany();
                if (company != null) {
                    getView().setCompanyName(company);
                }
                String postDate = response.getPostDate();
                if (postDate != null) {
                    String postedFormDate = DateUtils.getDateFromString(postDate);
                    getView().setPostDate(postedFormDate);
                }
                String expectDate = response.getExpectedArrivalTime();
                if (expectDate != null) {
                    String expectFormDate = DateUtils.getDateFromString(expectDate);
                    getView().setExpectedTime(expectFormDate);
                }
                String status = response.getStatus();
                if (status != null) {
                    getView().showStatus(status);
                }
            }
        }
    }


    public interface TrackView extends IView {


        void showFeedbackStatus();

        void showErrorInfo();

        void setTrackingNumber(String number);

        void setArtName(String name);

        void setCompanyName(String name);

        void setPostDate(String date);

        void setExpectedTime(String date);

        void switchFeedbackScreen();

        void showStatus(String status);

//        void setSaleId(long id);
    }

    private class TrackResultObserver extends DisposableObserver<PurchaseDetailModel> {

        @Override
        public void onNext(@NonNull PurchaseDetailModel response) {
            TrackPresenter.TrackView view = getView();
            if (view != null) {
                showResult(response);
                view.hideProgress();
            }
        }

        @Override
        public void onError(@NonNull Throwable e) {
            TrackPresenter.TrackView view = getView();
            if (view != null) {
                view.hideProgress();
                if (e instanceof CallServerException) {
                    view.showError(((CallServerException) e).getUserFriendlyErrorText(view.getContext().getResources()));
                }
            }
        }

        @Override
        public void onComplete() {
            //Nothing need here
        }
    }

    private class FeedbackResultObserver extends DisposableObserver<Object> {

        @Override
        public void onNext(Object response) {
            //Nothing to do
        }

        @Override
        public void onError(@NonNull Throwable e) {
            TrackPresenter.TrackView view = getView();
            if (view != null) {
                view.hideProgress();
                if (e instanceof CallServerException) {
                    view.showError(((CallServerException) e).getUserFriendlyErrorText(view.getContext().getResources()));
                }
            }
        }

        @Override
        public void onComplete() {
            TrackPresenter.TrackView view = getView();
            if (view != null) {
                view.hideProgress();
                view.showFeedbackStatus();
            }
        }
    }


}

