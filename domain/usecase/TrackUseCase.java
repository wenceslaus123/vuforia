package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.domain.model.PurchaseDetailModel;
import com.vuforia.vuforia.domain.repository.PurchaseDetailsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class TrackUseCase extends UseCase<PurchaseDetailModel, String> {


    private final PurchaseDetailsRepository repository;

    @Inject
    TrackUseCase(PurchaseDetailsRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<PurchaseDetailModel> buildUseCaseObservable(String s) {
        return repository.track(s);
    }
}
