package com.vuforia.vuforia.domain.repository;

import com.vuforia.vuforia.domain.model.FeedbackModel;

import io.reactivex.Observable;

public interface FeedbackRepository {
    Observable<Object> postFeedback(FeedbackModel model);
}
