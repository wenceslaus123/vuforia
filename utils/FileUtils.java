package com.vuforia.vuforia.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import timber.log.Timber;

public final class FileUtils {

    private FileUtils() {
        //Utils class
    }

    @NonNull
    public static File saveBitmapToInternalStorageGallery(Bitmap bmp, String filename, File path) throws IOException {
        File file = new File(path, filename);
        path.mkdirs();

        FileOutputStream fos = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.flush();
        fos.close();
        Timber.d(file.getPath());
        return file;
    }

    @NonNull
    public static String saveIO(InputStream io, String filename) throws Exception {
        try {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            File file = new File(path, filename);
            path.mkdirs();

            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[io.available()];
            io.read(data);
            os.write(data);
            io.close();
            os.close();
            Timber.d(file.getPath());
            return file.toString();
        } catch (IOException e) {
            try {
                io.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    @NonNull
    public static String saveFilePhotoToGallery(File inputFile, String filename) throws Exception {
        FileInputStream io = null;
        try {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File file = new File(path, filename);
            path.mkdirs();

            io = new FileInputStream(inputFile);

            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[io.available()];
            io.read(data);
            os.write(data);
            io.close();
            os.close();
            Timber.d(file.getPath());
            return file.toString();
        } catch (IOException e) {
            try {
                io.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            throw new Exception(e);
        }
    }
}
