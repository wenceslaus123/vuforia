package com.vuforia.vuforia.utils;


import android.content.Context;
import android.content.SharedPreferences;

import com.vuforia.vuforia.data.model.responce.Country;

import javax.inject.Inject;

public class PrefStorage {

    private static final String TERMS_ACCEPTED = "terms_accepted";

    private final SharedPreferences sp;

    @Inject
    public PrefStorage(Context context) {
        sp = context.getSharedPreferences("vuf.ua", Context.MODE_PRIVATE);
    }

    public void saveCountry(Country country) {
        sp.edit().putInt("country_id", country.getId())
                .putString("country_name", country.getName())
                .putString("country_curr", country.getCurrency())
                .apply();

    }

    public Country restoreCountry() {
        Country country = new Country();
        country.setId(sp.getInt("country_id", 5));
        country.setName(sp.getString("country_name", "USA"));
        country.setCurrency(sp.getString("country_curr", "USD"));
        return country;
    }

    public void saveTermsIsAccepted(boolean isAccepted) {
        sp.edit().putBoolean(TERMS_ACCEPTED, isAccepted).apply();
    }

    public boolean isTermsAccepted() {
        return sp.getBoolean(TERMS_ACCEPTED, false);
    }
}
