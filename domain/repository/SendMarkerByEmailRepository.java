package com.vuforia.vuforia.domain.repository;

import com.vuforia.vuforia.domain.model.MarkerModel;

import io.reactivex.Observable;

public interface SendMarkerByEmailRepository {
    Observable<Void> sendMarkerByEmail(MarkerModel model);
}
