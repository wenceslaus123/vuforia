package com.vuforia.vuforia.data.model.responce;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Charge {

    @SerializedName("importCharge")
    @Expose
    private String importCharge;

    public String getImportCharge() {
        return importCharge;
    }

    public void setImportCharge(String importCharge) {
        this.importCharge = importCharge;
    }
}
