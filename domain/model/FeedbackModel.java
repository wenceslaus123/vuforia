package com.vuforia.vuforia.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedbackModel implements Parcelable {

    public static final Creator<FeedbackModel> CREATOR = new Creator<FeedbackModel>() {
        @Override
        public FeedbackModel createFromParcel(Parcel in) {
            return new FeedbackModel(in);
        }

        @Override
        public FeedbackModel[] newArray(int size) {
            return new FeedbackModel[size];
        }
    };
    private final int saleId;
    private final boolean isBuyerHappy;
    private final String comment;
    private final int overallExperience;

    FeedbackModel(Builder builder) {
        saleId = builder.salId;
        isBuyerHappy = builder.isBuyerHappy;
        comment = builder.comment;
        overallExperience = builder.overallExperience;
    }

    protected FeedbackModel(Parcel in) {
        saleId = in.readInt();
        isBuyerHappy = in.readByte() != 0;
        comment = in.readString();
        overallExperience = in.readInt();
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(saleId);
        dest.writeByte((byte) (isBuyerHappy ? 1 : 0));
        dest.writeString(comment);
        dest.writeInt(overallExperience);
    }

    @Override
    public String toString() {
        return "FeedbackModel{" +
                "saleId=" + saleId +
                ", isBuyerHappy='" + isBuyerHappy + '\'' +
                ", comment=" + comment +
                ", overallExperience=" + overallExperience +
                '}';
    }


    public int getSaleId() {
        return saleId;
    }

    public boolean getBuyerHappy() {
        return isBuyerHappy;
    }

    public String getComment() {
        return comment;
    }

    public int getOverallExperience() {
        return overallExperience;
    }

    public static final class Builder {
        private int salId;
        private boolean isBuyerHappy;
        private String comment;
        private int overallExperience;

        public Builder() {
            //Nothing here
        }

        public Builder sailId(int val) {
            salId = val;
            return this;
        }

        public Builder buyerHappinessLevel(boolean val) {
            isBuyerHappy = val;
            return this;
        }

        public Builder comment(String val) {
            comment = val;
            return this;
        }

        public Builder overallExperience(int val) {
            overallExperience = val;
            return this;
        }

        public FeedbackModel build() {
            return new FeedbackModel(this);
        }

    }
}
