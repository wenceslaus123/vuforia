package com.vuforia.vuforia.presentation.ui.paypal;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.BuildConfig;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.model.responce.Charge;
import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.domain.model.Sale;
import com.vuforia.vuforia.presentation.ui.VuforiaBar;
import com.vuforia.vuforia.presentation.ui.BaseActivity;
import com.vuforia.vuforia.presentation.ui.SendBeamerActivity;
import com.vuforia.vuforia.presentation.ui.TrackPurchaseActivity;
import com.vuforia.vuforia.presentation.ui.beam.BeamActivity;
import com.vuforia.vuforia.presentation.ui.country.CountryActivity;
import com.vuforia.vuforia.presentation.ui.order.OrderDetailsActivity;
import com.vuforia.vuforia.utils.BeamerUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.inject.Inject;

import timber.log.Timber;

import static com.vuforia.vuforia.presentation.ui.country.CountryActivity.RESULT;

/**
 * Buy screen with info of art and PayPal
 */
public class BuyActivity extends BaseActivity<BuyPresenter.CountryView, BuyPresenter> implements BuyPresenter.CountryView {
    private static final String CONFIG_ENVIRONMENT = BuildConfig.PAYPAL_ENVIRONMENT;
    private static final String CONFIG_CLIENT_ID = "ARtAMUCdZGCOwrXHpJfT95elyoF8yZe6jcTB-dV6mrIvXj4u8Xq-Lf7-YdTieFT5_aNTLZdOAnNcm_Of";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int RESULT_CODE_COUNTRIES = 1010;

    ArtPiece artPiece;
    View progress;
    VuforiaBar vufBar;

    @Inject
    BuyPresenter presenter;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
//            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
//            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        artPiece = getIntent().getParcelableExtra(BeamActivity.IMAGE);
        VuforiaApplication.getComponent(this).inject(this);
        setTitle(artPiece.getName());
        initFields(artPiece);
        presenter.loadCharge(String.valueOf(artPiece.getId()));

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        findViewById(R.id.ab_buy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                presenter.reserveArt(artPiece.getId());
            }
        });

        findViewById(R.id.ab_beam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(artPiece.getImageUrl()) || !Patterns.WEB_URL.matcher(artPiece.getImageUrl()).matches()) {
                    showError(R.string.no_image_for_beam);
                    return;
                }
                showProgress();
                Glide.with(BuyActivity.this)
                        .load(artPiece.getImageUrl())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                hideProgress();
                                Intent intent;
                                if (BeamerUtils.isNeedGoToSendBEamerScreen(PreferenceManager.getDefaultSharedPreferences(BuyActivity.this))) {
                                    intent = new Intent(BuyActivity.this, SendBeamerActivity.class);
                                } else {
                                    intent = new Intent(BuyActivity.this, BeamActivity.class);
                                }
                                intent.putExtra(BeamActivity.IMAGE, artPiece);
                                startActivity(intent);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                hideProgress();
                                showError(R.string.failed_to_load_image);
                            }
                        });
            }
        });

        findViewById(R.id.ab_your_country).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(BuyActivity.this, CountryActivity.class), RESULT_CODE_COUNTRIES);
            }
        });

    }

    private void initFields(ArtPiece artPiece) {
        vufBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        vufBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        vufBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTrackPurchase = new Intent(BuyActivity.this,
                        TrackPurchaseActivity.class);
                intentTrackPurchase.putExtra("ACTIVITY_TO_LAUNCH",
                        "com.vuforia.vuforia.presentation.ui.paypal.BuyActivity");
                startActivity(intentTrackPurchase);
            }
        });
        progress = findViewById(R.id.fl_progress);
        Glide.with(this)

                .load(artPiece.getImageUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model,
                                               Target<GlideDrawable> target, boolean isFirstResource) {
                        findViewById(R.id.pb_image_progress).setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model,
                                                   Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        findViewById(R.id.pb_image_progress).setVisibility(View.GONE);
                        return false;
                    }
                })
                .into((ImageView) findViewById(R.id.ab_art));
        Glide.with(this).load(artPiece.getArtistPhoto()).placeholder(R.mipmap.userpic).into((ImageView) findViewById(R.id.iv_artist_photo));
        ((TextView) findViewById(R.id.ab_size)).setText(getString(R.string.size_desc,
                (artPiece.getBeamHeight() / 10),          // height to cm
                (artPiece.getBeamWidth() / 10),           // width to cm
                (int) (artPiece.getBeamHeight() / 10 / 2.54),   // height to inch
                (int) (artPiece.getBeamWidth() / 10 / 2.54))    // width to inch
        );
        ((TextView) findViewById(R.id.ab_medium)).setText(artPiece.getMediumType());
        ((TextView) findViewById(R.id.ab_framed)).setText(artPiece.isFramed() ? R.string.yes : R.string.no);
        ((TextView) findViewById(R.id.ab_artist)).setText(artPiece.getArtistName());
        ((TextView) findViewById(R.id.ab_room)).setText(Arrays.toString(artPiece.getSuitableRooms().toArray()).replace("[", "").replace("]", ""));
        ((TextView) findViewById(R.id.ab_room_desc)).setText(artPiece.getSuitableRooms().size() > 1 ? R.string.rooms : R.string.room);
        ((TextView) findViewById(R.id.ab_place)).setText(artPiece.getPlaceName());
        ((TextView) findViewById(R.id.ab_artist_country)).setText(artPiece.getArtistCountry());
        ((TextView) findViewById(R.id.ab_your_country)).setText(presenter.getCountry().getName());
        ((TextView) findViewById(R.id.ab_name)).setText(artPiece.getName());
        ((TextView) findViewById(R.id.tv_bio)).setText(artPiece.getBio());
        ((TextView) findViewById(R.id.ab_price)).setText(getString(R.string.order_desc, artPiece.getCurrency(), artPiece.getPrice()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm == null) {
                    presenter.releaseArt(artPiece.getId());
                } else {
                    showProgress();
                    presenter.saveTransaction(artPiece.getId(), confirm);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                presenter.releaseArt(artPiece.getId());
                Timber.i("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                presenter.releaseArt(artPiece.getId());
                Timber.i("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == RESULT_CODE_COUNTRIES && resultCode == Activity.RESULT_OK) {
            Country country = data.getParcelableExtra(RESULT);
            ((TextView) findViewById(R.id.ab_your_country)).setText(country.getName());
            presenter.setCountry(country);
            showProgress();
            presenter.loadCharge(String.valueOf(artPiece.getId()));
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void showImportCharge(@Nullable Charge charge) {
        ((TextView) findViewById(R.id.ab_import_charge)).setText(charge == null ? "-" : charge.getImportCharge());
    }

    @Override
    public void nextStep(String data, Sale sale) {
        Intent intent = new Intent(this, OrderDetailsActivity.class);
        intent.putExtra(OrderDetailsActivity.EXTRA_REFERENCE_NUMBER, data);
        intent.putExtra(OrderDetailsActivity.EXTRA_SALE, sale);
        startActivity(intent);
        finish();
    }

    @Override
    public void reserveComplete() {
        PayPalPayment artToBuy = new PayPalPayment(new BigDecimal(artPiece.getPrice()),
                artPiece.getCurrency(), artPiece.getName(), PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(BuyActivity.this, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, artToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @NonNull
    @Override
    protected BuyPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected BuyPresenter.CountryView getView() {
        return this;
    }
}
