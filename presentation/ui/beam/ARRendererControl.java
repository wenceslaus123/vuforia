package com.vuforia.vuforia.presentation.ui.beam;

import com.vuforia.State;


interface ARRendererControl {
    void renderFrame(State state, float... projectionMatrix);
}
