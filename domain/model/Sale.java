package com.vuforia.vuforia.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Sale implements Parcelable {
    public long artpieceId;
    public String email;
    public int destinationCountryId;
    public String transactionStatus;
    public String transactionId;
    public String transactionDate;

    public Sale() {
        //DO NOTHING
    }

    Sale(Builder builder) {
        artpieceId = builder.artpieceId;
        email = builder.email;
        destinationCountryId = builder.destinationCountryId;
        transactionStatus = builder.transactionStatus;
        transactionId = builder.transactionId;
        transactionDate = builder.transactionDate;
    }

    public void resetTransaction() {
        transactionStatus = null;
        transactionId = null;
        transactionDate = null;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private long artpieceId;
        private String email;
        private int destinationCountryId;
        private String transactionStatus;
        private String transactionId;
        private String transactionDate;

        Builder() {
            //DO NOTHING
        }

        public Builder artpieceId(long val) {
            artpieceId = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder destinationCountryId(int val) {
            destinationCountryId = val;
            return this;
        }

        public Builder transactionStatus(String val) {
            transactionStatus = val;
            return this;
        }

        public Builder transactionId(String val) {
            transactionId = val;
            return this;
        }

        public Builder transactionDate(String val) {
            transactionDate = val;
            return this;
        }

        public Sale build() {
            return new Sale(this);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.artpieceId);
        dest.writeString(this.email);
        dest.writeInt(this.destinationCountryId);
        dest.writeString(this.transactionStatus);
        dest.writeString(this.transactionId);
        dest.writeString(this.transactionDate);
    }

    protected Sale(Parcel in) {
        this.artpieceId = in.readLong();
        this.email = in.readString();
        this.destinationCountryId = in.readInt();
        this.transactionStatus = in.readString();
        this.transactionId = in.readString();
        this.transactionDate = in.readString();
    }

    public static final Parcelable.Creator<Sale> CREATOR = new Parcelable.Creator<Sale>() {
        @Override
        public Sale createFromParcel(Parcel source) {
            return new Sale(source);
        }

        @Override
        public Sale[] newArray(int size) {
            return new Sale[size];
        }
    };
}
