package com.vuforia.vuforia.data.token;

public interface TokenStorage {
    String getCookies();

    void setCookies(String cookies);

    void clearCookies();
}
