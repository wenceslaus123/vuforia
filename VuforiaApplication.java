package com.vuforia.vuforia;

import android.app.Application;
import android.content.Context;

import com.vuforia.vuforia.dagger.VufComponent;
import com.vuforia.vuforia.dagger.DaggerVuforiaComponent;
import com.vuforia.vuforia.dagger.DataModule;

import timber.log.Timber;

public class VuforiaApplication extends Application {

    private VufComponent component;

    public static VufComponent getComponent(Context context) {
        return ((VuforiaApplication) context.getApplicationContext()).component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FlavorApp.onApplicationCreate(this);
        component = DaggerVuforiaComponent.builder()
                .dataModule(new DataModule(this))
                .build();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
