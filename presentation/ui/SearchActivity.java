package com.vuforia.vuforia.presentation.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.presentation.presenter.SearchPresenter;
import com.vuforia.vuforia.presentation.ui.beam.BeamActivity;
import com.vuforia.vuforia.presentation.ui.paypal.BuyActivity;
import com.vuforia.vuforia.utils.BeamerUtils;
import com.vuforia.vuforia.utils.DeviceUtils;
import com.vuforia.vuforia.utils.EditTextDrawableHandler;
import com.vuforia.vuforia.utils.KeyBoardUtils.KeyBoardVisibilityChange;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

import static com.vuforia.vuforia.utils.KeyBoardUtils.glueKeyBoardWatcher;

public class SearchActivity extends BaseActivity<SearchPresenter.SearchView, SearchPresenter>
        implements SearchPresenter.SearchView, ArtPieceAdapter.ItemClickListener, KeyBoardVisibilityChange {

    public static final int PERMISSIONS_REQUEST = 101;

    @Inject
    SearchPresenter presenter;
    private EditText evSearch;

    private FrameLayout flSearch;
    private FrameLayout flResult;
    private FrameLayout flProgress;
    private RecyclerView rvResult;
    private VuforiaBar actionBar;
    private ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        evSearch = (EditText) findViewById(R.id.ev_search);
        logo = (ImageView) findViewById(R.id.si_image);
        flResult = (FrameLayout) findViewById(R.id.fl_result);
        rvResult = (RecyclerView) findViewById(R.id.rv_result);
        flSearch = (FrameLayout) findViewById(R.id.fl_search);
        flProgress = (FrameLayout) findViewById(R.id.fl_progress);
        actionBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        actionBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flResult.getVisibility() == View.VISIBLE) {
                    onBackPressed();
                } else {
                    showError(R.string.tooltip_search);
                }
            }
        });
        actionBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchActivity.this, TrackPurchaseActivity.class));
            }
        });

        evSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        evSearch.setOnTouchListener(new EditTextDrawableHandler(evSearch, new EditTextDrawableHandler.ButtonPressListener() {
            @Override
            public void onButtonPressed() {
                search();
            }
        }));

        VuforiaApplication.getComponent(this).inject(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvResult.setLayoutManager(layoutManager);
        rvResult.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            @SuppressWarnings("PMD")
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (flProgress.getVisibility() != View.VISIBLE && !presenter.isLastPage()) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount &&
                            firstVisibleItemPosition >= 0 && totalItemCount >= presenter.getPageSize()) {
                        presenter.loadMore();
                    }
                }
            }
        });
        glueKeyBoardWatcher(findViewById(R.id.root), this);
    }

    @NonNull
    @Override
    protected SearchPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected SearchPresenter.SearchView getView() {
        return this;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                nextScreen(presenter.getSelectedArtPiece(), presenter.getActionType());
            } else {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.permissions_error)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (flResult.getVisibility() == View.VISIBLE) {
            flResult.setVisibility(View.GONE);
            flSearch.setVisibility(View.VISIBLE);
            actionBar.hideTitle();
            actionBar.setHome(R.mipmap.top_bar_icon_info);
            evSearch.setEnabled(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showResult(List<ArtPiece> data) {
        flProgress.setVisibility(View.GONE);
        flSearch.setVisibility(View.GONE);
        flResult.setVisibility(View.VISIBLE);
        evSearch.setEnabled(false);


        actionBar.setTitle(evSearch.getText().toString().isEmpty() ?
                getString(R.string.search_result_empty) :
                getString(R.string.search_results, evSearch.getText().toString()));
        actionBar.setHome(R.mipmap.top_bar_icon_back);

        ((TextView) flResult.findViewById(R.id.tv_result_count))
                .setText(String.format(Locale.US, "%d %s", getPresenter().getTotalCount(),
                        getResources().getQuantityString(R.plurals.format_count_results, presenter.getTotalCount())));
        RecyclerView.Adapter adapter = rvResult.getAdapter();
        if (adapter == null) {
            adapter = new ArtPieceAdapter(data);
        } else {
            ArtPieceAdapter adapter1 = (ArtPieceAdapter) adapter;
            adapter1.clear();
            adapter1.setData(data);
        }
        ((ArtPieceAdapter) adapter).setItemClickListener(this);
        rvResult.setAdapter(adapter);

        DeviceUtils.hideSoftKeyboard(evSearch);
    }

    @Override
    public void updateResult(List<ArtPiece> data) {
        ((ArtPieceAdapter) rvResult.getAdapter()).updateData(data);
    }

    @Override
    public void onItemClicked(final ArtPiece artPiece, int actionType) {
        presenter.setSelectedArtPiece(artPiece, actionType);
        if (ContextCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            nextScreen(artPiece, actionType);
        } else {
            ActivityCompat.requestPermissions(SearchActivity.this, new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void showProgress() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flProgress.setVisibility(View.GONE);
    }

    public void search() {
        presenter.search(evSearch.getText().toString());
    }

    private void nextScreen(ArtPiece artPiece, int actionType) {
        if (actionType == TYPE_BEAM) {
            openBeamScreen(artPiece);
        } else {
            openBuyScreen(artPiece);
        }
    }

    private void openBeamScreen(final ArtPiece artPiece) {
        if (TextUtils.isEmpty(artPiece.getImageUrl()) || !Patterns.WEB_URL.matcher(artPiece.getImageUrl()).matches()) {
            showError(R.string.no_image_for_beam);
            return;
        }
        showProgress();
        Glide.with(this)
                .load(artPiece.getImageUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        hideProgress();
                        Intent intent;
                        if (BeamerUtils.isNeedGoToSendBEamerScreen(PreferenceManager.getDefaultSharedPreferences(SearchActivity.this))) {
                            intent = new Intent(SearchActivity.this, SendBeamerActivity.class);
                            intent.putExtra(BeamActivity.IMAGE, artPiece);
                            intent.putExtra("TITLE_NAME", getString(R.string.title_send_marker_screen));
                            startActivityForResult(intent, 1);
                        } else {
                            intent = new Intent(SearchActivity.this, BeamActivity.class);
                            intent.setExtrasClassLoader(ArtPiece.class.getClassLoader());
                            intent.putExtra(BeamActivity.IMAGE, artPiece);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        Timber.e(e.getMessage());
                        hideProgress();
                        showError(R.string.failed_to_load_image);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
        Intent intent = new Intent(SearchActivity.this, BeamActivity.class);
        intent.setExtrasClassLoader(ArtPiece.class.getClassLoader());
        intent.putExtra(BeamActivity.IMAGE, presenter.getSelectedArtPiece());
        startActivity(intent);
//        }
    }

    private void openBuyScreen(final ArtPiece artPiece) {
        Intent intent = new Intent(SearchActivity.this, BuyActivity.class);
        intent.setExtrasClassLoader(ArtPiece.class.getClassLoader());
        intent.putExtra(BeamActivity.IMAGE, artPiece);
        startActivity(intent);
    }

    @Override
    public void keyBoardVisibilityChange(boolean visible) {
        logo.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
    }
}