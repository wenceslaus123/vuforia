package com.vuforia.vuforia.data.model.responce;

import com.google.gson.annotations.SerializedName;

public class TokenResponse {
    @SerializedName("token")
    String token;

    public String getToken() {
        return token;
    }
}
