package com.vuforia.vuforia.utils;

import android.text.Editable;
import android.text.TextWatcher;

public class TextChangeListener implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //DO NOTHING
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //DO NOTHING
    }

    @Override
    public void afterTextChanged(Editable s) {
        onTextChange(s.toString());
    }

    public void onTextChange(String text) {
        //Override in child
        //It must be abstract, but "An empty method in an abstract class should be abstract instead"
    }
}
