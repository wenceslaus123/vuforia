package com.vuforia.vuforia.presentation.ui.country;

import android.support.annotation.NonNull;

import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.domain.usecase.CountryUseCase;
import com.vuforia.vuforia.presentation.presenter.IView;
import com.vuforia.vuforia.presentation.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

class CountryPresenter extends Presenter<CountryPresenter.CountryView> {

    private final CountryUseCase countryUseCase;

    @Inject
    CountryPresenter(CountryUseCase countryUseCase) {
        this.countryUseCase = countryUseCase;
    }

    @Override
    protected void onAttachView(@NonNull CountryView view) {
        countryUseCase.execute(new CountryDisposableObserver(), null);
    }

    interface CountryView extends IView {
        void showResult(List<Country> data);
    }

    private class CountryDisposableObserver extends DisposableObserver<List<Country>> {

        @Override
        public void onNext(@NonNull List<Country> countries) {
            if (getView() != null) {
                getView().showResult(countries);
            }
        }

        @Override
        public void onError(@NonNull Throwable e) {
            //DO NOTHING
        }

        @Override
        public void onComplete() {
            //DO NOTHING
        }
    }
}
