package com.vuforia.vuforia.presentation.ui.beam;

import android.opengl.GLES20;

import timber.log.Timber;


final class ARUtils {

    private ARUtils() {
        //Utils class
    }

    private static int initShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        if (shader != 0) {
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);

            int[] glStatusVar = {GLES20.GL_FALSE};
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, glStatusVar,
                    0);
            if (glStatusVar[0] == GLES20.GL_FALSE) {
                Timber.e("Could NOT compile shader %d : %s", shaderType, GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);
                shader = 0;
            }
        }

        return shader;
    }

    static int createProgramFromShaderSrc(String vertexShaderSrc, String fragmentShaderSrc) {
        int vertShader = initShader(GLES20.GL_VERTEX_SHADER, vertexShaderSrc);
        int fragShader = initShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderSrc);

        if (vertShader == 0 || fragShader == 0) {
            return 0;
        }

        int program = GLES20.glCreateProgram();
        if (program != 0) {
            GLES20.glAttachShader(program, vertShader);
            checkGLError("glAttchShader(vert)");

            GLES20.glAttachShader(program, fragShader);
            checkGLError("glAttchShader(frag)");

            GLES20.glLinkProgram(program);
            int[] glStatusVar = {GLES20.GL_FALSE};
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, glStatusVar,
                    0);
            if (glStatusVar[0] == GLES20.GL_FALSE) {
                Timber.e("Could NOT link program : %s", GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);
                program = 0;
            }
        }

        return program;
    }

    static void checkGLError(String op) {
        for (int error = GLES20.glGetError(); error != 0; error = GLES20.glGetError()) {
            Timber.e("After operation %s got glError 0x%s", op, Integer.toHexString(error));
        }
    }
}
