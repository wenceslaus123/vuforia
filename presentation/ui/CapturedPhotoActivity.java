package com.vuforia.vuforia.presentation.ui;

import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.utils.FileUtils;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import timber.log.Timber;


public class CapturedPhotoActivity extends AppCompatActivity {

    public static final String CAPTURED_PHOTO_PATH = "captured_photo_path";
    private static final int REQUEST_CODE = 301;
    private static final String SHARE_FILE_DELETED_RESULT = "share file deleted result = %s";

    private File shareFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captured_photo);

        VuforiaBar vufBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        vufBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Timber.i(SHARE_FILE_DELETED_RESULT, shareFile.delete());
                finishAffinity();
            }
        });
        vufBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        vufBar.hideMarker();

        ImageView ivPhoto = (ImageView) findViewById(R.id.iv_captured_photo);

        shareFile = (File) getIntent().getSerializableExtra(CAPTURED_PHOTO_PATH);
        Glide.with(this).load(shareFile).into(ivPhoto);

        findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MediaScannerConnection.scanFile(CapturedPhotoActivity.this, new String[]{saveToExternalDir()},
                            null, new MediaScannerConnection.MediaScannerConnectionClient() {
                                @Override
                                public void onMediaScannerConnected() {
                                    //Nothing need here
                                }

                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    share(uri);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(CapturedPhotoActivity.this, R.string.failed_share_beamer, Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MediaScannerConnection.scanFile(CapturedPhotoActivity.this, new String[]{saveToExternalDir()},
                            null, new MediaScannerConnection.MediaScannerConnectionClient() {
                                @Override
                                public void onMediaScannerConnected() {
                                    //Nothing need here
                                }

                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Timber.i("Vuforia_Photo saved: %s", path);
                                    Timber.i(SHARE_FILE_DELETED_RESULT, shareFile.delete());
                                    finishAffinity();
                                }
                            });

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(CapturedPhotoActivity.this, R.string.failed_share_beamer, Toast.LENGTH_SHORT).show();
                }
//                if (shareFile.delete()) {
//                    Toast.makeText(CapturedPhotoActivity.this, "Photo deleted", Toast.LENGTH_SHORT).show();
//                    onBackPressed();
//                } else {
//                    Toast.makeText(CapturedPhotoActivity.this, "Failed to delete photo", Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    @NonNull
    public String saveToExternalDir() throws Exception {
        return FileUtils.saveFilePhotoToGallery(shareFile, String.format("Vuforia_Photo_%s.png",
                DateFormat.getDateTimeInstance().format(new Date())));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            Timber.i(SHARE_FILE_DELETED_RESULT, shareFile.delete());
            finishAffinity();
        }
    }

    @Override
    public void onBackPressed() {
        Timber.i(SHARE_FILE_DELETED_RESULT, shareFile.delete());
        super.onBackPressed();
    }

    private void share(Uri file) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, file);
        startActivityForResult(Intent.createChooser(shareIntent, "Share your image via ..."), REQUEST_CODE);
    }
}