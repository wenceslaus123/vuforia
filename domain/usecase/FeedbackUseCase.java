package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.domain.model.FeedbackModel;
import com.vuforia.vuforia.domain.repository.FeedbackRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class FeedbackUseCase extends UseCase<Object, FeedbackModel> {
    private final FeedbackRepository repository;

    @Inject
    FeedbackUseCase(FeedbackRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<Object> buildUseCaseObservable(FeedbackModel feedbackModel) {
        return repository.postFeedback(feedbackModel);
    }
}
