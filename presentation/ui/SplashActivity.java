package com.vuforia.vuforia.presentation.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.presentation.presenter.SplashPresenter;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<SplashPresenter.SplashView, SplashPresenter> implements SplashPresenter.SplashView {

    @Inject
    SplashPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VuforiaApplication.getComponent(this).inject(this);

        setContentView(R.layout.activity_splash);
    }

    @Override
    public void showProgress() {
        //nothing need
    }

    @Override
    public void hideProgress() {
        //nothing need
    }

    @NonNull
    @Override
    protected SplashPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected SplashPresenter.SplashView getView() {
        return this;
    }

    @Override
    public void goToApp() {
        startActivity(new Intent(this, SearchActivity.class));
        finish();
    }
}
