package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.data.repository.SaleRepository;
import com.vuforia.vuforia.domain.model.Sale;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SaleUseCase extends UseCase<String, Sale> {

    private final SaleRepository repository;

    @Inject
    SaleUseCase(SaleRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<String> buildUseCaseObservable(Sale sale) {
        return repository.sendSale(sale);
    }
}
