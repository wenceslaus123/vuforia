package com.vuforia.vuforia.data;

import com.vuforia.vuforia.data.model.responce.BaseResponse;

import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class ExtractDataFromApiResponse<T> implements ObservableTransformer<BaseResponse<T>, T> {

    @Override
    public ObservableSource<T> apply(@NonNull io.reactivex.Observable<BaseResponse<T>> upstream) {
        return upstream.map(new Function<BaseResponse<T>, T>() {
            @Override
            public T apply(@NonNull BaseResponse<T> apiResponse) throws Exception {
                if (apiResponse.success && apiResponse.statusCode == 200) {
                    return apiResponse.data;
                } else {
                    throw new RuntimeException("Non 200 API status code: " + apiResponse.errorMessage);
                }
            }
        });
    }

}
