package com.vuforia.vuforia.presentation.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.vuforia.vuforia.R;

@SuppressWarnings("PMD")
public class VuforiaBar extends FrameLayout {

    private TextView title;
    private TextView textAction;
    private ImageView action;
    private ImageView home;
    private ImageView marker;

    public VuforiaBar(@NonNull Context context) {
        super(context);
        init(null);
    }

    public VuforiaBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public VuforiaBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public VuforiaBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        LayoutInflater.from(getContext()).inflate(R.layout.layout_action_bar, this, true);

        home = (ImageView) findViewById(R.id.iv_home);
        action = (ImageView) findViewById(R.id.iv_action);
        marker = (ImageView) findViewById(R.id.iv_marker);
        title = (TextView) findViewById(R.id.tv_title);
        textAction = (TextView) findViewById(R.id.tv_text_action);

        if (attrs == null) {
            return;
        }

        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.VuforiaBar, 0, 0);

        try {
            int action = a.getResourceId(R.styleable.VuforiaBar_action, -1);
            int home = a.getResourceId(R.styleable.VuforiaBar_home, -1);
            int title = a.getResourceId(R.styleable.VuforiaBar_title_scr, -1);
            int textAction = a.getResourceId(R.styleable.VuforiaBar_text_action, -1);

            if (action == -1) {
                hideAction();
            } else {
                setAction(action);
            }

            if (home == -1) {
                hideHome();
            } else {
                setHome(home);
            }

            if (title == -1) {
                hideTitle();
            } else {
                setTitle(title);
            }

            if (textAction > -1) {
                this.action.setVisibility(View.GONE);
                this.textAction.setVisibility(View.VISIBLE);
                this.textAction.setText(textAction);
            }

        } finally {
            a.recycle();
        }

        marker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SendBeamerActivity.class);
                intent.putExtra("TITLE_NAME", getResources().getString(R.string.title_send_marker_help_screen));
                getContext().startActivity(intent);
            }
        });
    }

    public void setTitle(int title) {
        this.title.setVisibility(VISIBLE);
        this.title.setText(title);
    }

    public void setTitle(String title) {
        this.title.setVisibility(VISIBLE);
        this.title.setText(title);
    }

    public void hideTitle() {
        this.title.setVisibility(GONE);
    }

    public void setHome(@DrawableRes int home) {
        this.home.setVisibility(VISIBLE);
        this.home.setImageResource(home);
    }

    public void hideMarker() {
        marker.setVisibility(GONE);
    }

    public void hideHome() {
        this.home.setVisibility(GONE);
    }

    public void setAction(@DrawableRes int action) {
        this.action.setVisibility(VISIBLE);
        this.action.setImageResource(action);
    }

    public void hideAction() {
        this.action.setVisibility(GONE);
    }

    public void setHomeListener(OnClickListener homeListener) {
        home.setOnClickListener(homeListener);
    }

    public void setActionListener(OnClickListener actionListener) {
        if (action.getVisibility() == View.VISIBLE) {
            action.setOnClickListener(actionListener);
        } else {
            textAction.setOnClickListener(actionListener);
        }
    }
}
