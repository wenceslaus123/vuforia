package com.vuforia.vuforia.presentation.ui.paypal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.data.model.responce.Charge;
import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.domain.model.Sale;
import com.vuforia.vuforia.domain.usecase.ChargeUseCase;
import com.vuforia.vuforia.domain.usecase.ReleaseArtUseCase;
import com.vuforia.vuforia.domain.usecase.ReserveArtUseCase;
import com.vuforia.vuforia.domain.usecase.SaleUseCase;
import com.vuforia.vuforia.presentation.presenter.IView;
import com.vuforia.vuforia.presentation.presenter.Presenter;
import com.vuforia.vuforia.utils.PrefStorage;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

class BuyPresenter extends Presenter<BuyPresenter.CountryView> {

    private final SaleUseCase saleUseCase;
    private final ChargeUseCase chargeUseCase;
    private final ReserveArtUseCase reserveArtUseCase;
    private final ReleaseArtUseCase releaseArtUseCase;
    private final PrefStorage prefStorage;

    private Country country;
    private Sale sale;

    @Inject
    BuyPresenter(ChargeUseCase chargeUseCase, SaleUseCase saleUseCase, ReserveArtUseCase reserveArtUseCase,
                 ReleaseArtUseCase releaseArtUseCase, PrefStorage prefStorage) {
        this.chargeUseCase = chargeUseCase;
        this.saleUseCase = saleUseCase;
        this.reserveArtUseCase = reserveArtUseCase;
        this.releaseArtUseCase = releaseArtUseCase;
        this.prefStorage = prefStorage;
        this.country = prefStorage.restoreCountry();
    }

    void loadCharge(String artId) {
        chargeUseCase.withData(artId, String.valueOf(country.getId())).execute(new ChargeDisposableObserver(), null);
    }

    public void reserveArt(long artId) {
        reserveArtUseCase.execute(new ReserveArtDisposableObserver(), artId);
    }

    public void releaseArt(long artId) {
        releaseArtUseCase.execute(new ReleaseArtDisposableObserver(), artId);
    }

    void saveTransaction(long id, PaymentConfirmation confirm) {
        try {
            Timber.i(confirm.toJSONObject().toString(4));
            Timber.i(confirm.getPayment().toJSONObject().toString(4));
            JSONObject jsonObject = confirm.toJSONObject().getJSONObject("response");
            sale = Sale.newBuilder()
                    .artpieceId(id)
                    .destinationCountryId(country.getId())
                    .transactionDate(jsonObject.getString("create_time"))
                    .transactionId(jsonObject.getString("id"))
                    .transactionStatus(jsonObject.getString("state"))
                    .build();
            saleUseCase.execute(new TransactionDisposableObserver(), sale);
        } catch (JSONException e) {
            if (getView() != null) {
                getView().showError(R.string.error_parse_transaction);
            }
        }
    }

    Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
        prefStorage.saveCountry(country);
    }

    interface CountryView extends IView {
        void showImportCharge(@Nullable Charge data);

        void nextStep(String data, Sale sale);

        void reserveComplete();
    }

    private class ChargeDisposableObserver extends DisposableObserver<Charge> {

        @Override
        public void onNext(@NonNull Charge charge) {
            if (getView() != null) {
                getView().hideProgress();
                getView().showImportCharge(charge);
            }
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (getView() != null) {
                getView().hideProgress();
                getView().showImportCharge(null);
            }
        }

        @Override
        public void onComplete() {
            //DO NOTHING
        }
    }

    private class TransactionDisposableObserver extends DisposableObserver<String> {
        @Override
        public void onNext(@NonNull String s) {
            if (getView() != null) {
                getView().hideProgress();
                getView().nextStep(s, sale);
            }
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (getView() != null) {
                getView().hideProgress();
                getView().showError(R.string.error_save_transaction);
            }
        }

        @Override
        public void onComplete() {
            //DO NOTHING
        }
    }

    private class ReserveArtDisposableObserver extends DisposableObserver<Void> {
        @Override
        public void onNext(@NonNull Void v) {
            //DO NOTHING
            //REASON:
            //RXJava can't pass here null object at all
            //See onComplete()
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (getView() != null) {
                getView().hideProgress();
                if (e instanceof CallServerException) {
                    getView().showError((CallServerException) e);
                } else {
                    getView().showError(R.string.error_save_transaction);
                }
            }
        }

        @Override
        public void onComplete() {
            if (getView() != null) {
                getView().hideProgress();
                getView().reserveComplete();
            }
        }
    }

    private class ReleaseArtDisposableObserver extends DisposableObserver<Void> {
        @Override
        public void onNext(@NonNull Void v) {
            //DO NOTHING
            //REASON:
            //RXJava can't pass here null object at all
            //See onComplete()
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (getView() != null) {
                getView().hideProgress();
            }
        }

        @Override
        public void onComplete() {
            if (getView() != null) {
                getView().hideProgress();
            }
        }
    }
}
