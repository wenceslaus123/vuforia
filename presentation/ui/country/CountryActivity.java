package com.vuforia.vuforia.presentation.ui.country;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.presentation.ui.BaseActivity;

import java.util.List;

import javax.inject.Inject;

public class CountryActivity extends BaseActivity<CountryPresenter.CountryView, CountryPresenter>
        implements CountryPresenter.CountryView, CountryAdapter.ItemClickListener {

    public static final String RESULT = "result_country";

    @Inject
    CountryPresenter presenter;
    RecyclerView recyclerView;
    CountryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
        setContentView(R.layout.activity_country);
        VuforiaApplication.getComponent(this).inject(this);

        adapter = new CountryAdapter();
        adapter.setItemClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.ac_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
    }

    @Override
    public void showProgress() {
        //DO NOTHING
    }

    @Override
    public void hideProgress() {
        //DO NOTHING
    }

    @Override
    public void showResult(List<Country> data) {
        adapter.updateData(data);
    }

    @NonNull
    @Override
    protected CountryPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected CountryPresenter.CountryView getView() {
        return this;
    }

    @Override
    public void onItemClicked(Country country) {
        Intent intent = new Intent();
        intent.putExtra(RESULT, country);
        setResult(RESULT_OK, intent);
        onBackPressed();
    }
}
