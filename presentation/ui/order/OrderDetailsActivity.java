package com.vuforia.vuforia.presentation.ui.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.domain.model.Sale;
import com.vuforia.vuforia.presentation.ui.VuforiaBar;
import com.vuforia.vuforia.presentation.ui.TrackPurchaseActivity;

public class OrderDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_REFERENCE_NUMBER = "extra_reference_number";
    public static final String EXTRA_SALE = "extra_sale";

    VuforiaBar vufBar;
    String referenceNumber;
    Sale sale;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        if (getIntent() != null && getIntent().hasExtra(EXTRA_REFERENCE_NUMBER)) {
            referenceNumber = getIntent().getStringExtra(EXTRA_REFERENCE_NUMBER);
            sale = getIntent().getParcelableExtra(EXTRA_SALE);
        }
        initFields();
    }

    private void initFields() {
        vufBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        vufBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        vufBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTrackPurchase = new Intent(OrderDetailsActivity.this,
                        TrackPurchaseActivity.class);
                intentTrackPurchase.putExtra(TrackPurchaseActivity.EXTRA_CODE, referenceNumber);
                startActivity(intentTrackPurchase);
            }
        });
        ((TextView) findViewById(R.id.aod_reference_number)).setText(Html.fromHtml(getString(R.string.reference_number, referenceNumber)));
    }
}
