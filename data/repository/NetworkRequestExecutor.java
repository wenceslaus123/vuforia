package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.data.model.responce.BaseResponse;
import com.google.gson.Gson;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class NetworkRequestExecutor<T> implements ObservableOnSubscribe<T> {

    private final Call<BaseResponse<T>> apiCall;

    public NetworkRequestExecutor(Call<BaseResponse<T>> apiCall) {
        this.apiCall = apiCall;
    }

    @Override
    public void subscribe(@NonNull ObservableEmitter<T> emitter) throws Exception {
        try {
            Response<BaseResponse<T>> execute = apiCall.execute();
            if (execute.isSuccessful()) {
                BaseResponse<T> body = execute.body();
                if (body == null) {
                    Timber.d("Request successful, but response is empty");
                    emitter.onError(new CallServerException("Request successful, but response is empty", R.string.internal_server_error));
                } else {
                    if (body.success && body.statusCode == 200) {
                        if (body.data != null) {
                            emitter.onNext(body.data);
                        }
                        emitter.onComplete();
                    } else {
                        Timber.d(body.errorMessage);
                        emitter.onError(new CallServerException(body.errorMessage, body.errorMessage));
                    }
                }
            } else {
                ResponseBody responseBody = execute.errorBody();
                if (responseBody == null) {
                    Timber.d("Request failed, error body is empty");
                    emitter.onError(new CallServerException("Request failed, error body is empty", R.string.internal_server_error));
                } else {
                    BaseResponse baseResponse = new Gson().fromJson(responseBody.string(), BaseResponse.class);
                    Timber.d(responseBody.string());
                    emitter.onError(new CallServerException(baseResponse.errorMessage, baseResponse.errorMessage));
                }
            }
        } catch (Exception e) {
            Timber.e(e);
            if (!emitter.isDisposed()) {
                emitter.onError(new CallServerException(e.getMessage(), R.string.internal_server_error));
            }
        }
    }
}