package com.vuforia.vuforia.data.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {

    @SerializedName("statusCode")
    @Expose
    public long statusCode;

    @SerializedName("success")
    @Expose
    public boolean success;

    @SerializedName("errorMessage")
    @Expose
    public String errorMessage;

    @SerializedName("data")
    @Expose
    public T data;

}
