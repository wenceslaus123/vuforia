package com.vuforia.vuforia.utils;

import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;


public class EditTextDrawableHandler implements View.OnTouchListener {


    private final static int DRAWABLE_RIGHT = 2;

    private final EditText evText;
    private final ButtonPressListener listener;

    public EditTextDrawableHandler(@NonNull EditText evText, @NonNull ButtonPressListener listener) {
        this.evText = evText;
        this.listener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP &&
                event.getRawX() >= (evText.getRight() - evText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
            listener.onButtonPressed();
            v.performClick();
            return true;
        }
        return false;
    }

    public interface ButtonPressListener {
        void onButtonPressed();
    }
}
