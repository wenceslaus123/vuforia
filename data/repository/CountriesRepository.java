package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.data.model.responce.Charge;
import com.vuforia.vuforia.data.model.responce.Country;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CountriesRepository {
    private final Api api;

    @Inject
    public CountriesRepository(Api api) {
        this.api = api;
    }

    public Observable<List<Country>> getCountries() {
        return Observable.create(new NetworkRequestExecutor<>(api.getCountries()));
    }

    public Observable<Charge> getCharge(String artId, String countryId) {
        return Observable.create(new NetworkRequestExecutor<>(api.getCharge(artId, countryId)));
    }
}
