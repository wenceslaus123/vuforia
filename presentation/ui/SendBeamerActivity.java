package com.vuforia.vuforia.presentation.ui;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.presentation.presenter.SendMarkerByEmailPresenter;
import com.vuforia.vuforia.presentation.ui.beam.BeamActivity;
import com.vuforia.vuforia.utils.PrefStorage;

import javax.inject.Inject;

import static com.vuforia.vuforia.utils.DeviceUtils.hideSoftKeyboard;

public class SendBeamerActivity extends BaseActivity<SendMarkerByEmailPresenter.SendMarkerView, SendMarkerByEmailPresenter>
        implements SendMarkerByEmailPresenter.SendMarkerView, View.OnClickListener {

    public static final int REQUEST_CODE = 201;
    public static final int REQUEST_CODE_TERMS = 2;
    public static final int REQUEST_CODE_PURCHASE = 3;
    private Button btnSendBeamer;
    private EditText evEmail;
    private ArtPiece artPiece;
    private CheckBox chbTerms;
    private ProgressBar flProgress;
    @Inject
    SendMarkerByEmailPresenter presenter;
    @Inject
    PrefStorage prefStorage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_beamer);
        VuforiaBar actionBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        Bundle extras = getIntent().getExtras();
        actionBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        actionBar.setTitle(extras.getString("TITLE_NAME"));
        actionBar.hideMarker();
        artPiece = getIntent().getParcelableExtra(BeamActivity.IMAGE);
        btnSendBeamer = (Button) findViewById(R.id.btn_send_beamer);
        chbTerms = (CheckBox) findViewById(R.id.chb_terms_conditions);
        chbTerms.setOnClickListener(this);
        flProgress = (ProgressBar) findViewById(R.id.transp_progress);
        btnSendBeamer.setActivated(true);
        btnSendBeamer.setOnClickListener(this);
        evEmail = (EditText) findViewById(R.id.et_email);
        VuforiaApplication.getComponent(this).inject(this);
        if (prefStorage.isTermsAccepted()) {
            chbTerms.setActivated(true);
        } else {
            findViewById(R.id.item_agree_terms).setVisibility(View.VISIBLE);
            btnSendBeamer.setActivated(false);
        }

    }

    @Override
    @SuppressLint("ConfusingTernary")
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chb_terms_conditions:
                if (chbTerms.isActivated()) {
                    chbTerms.setActivated(false);
                    btnSendBeamer.setActivated(false);
                } else {
                    chbTerms.setActivated(true);
                    btnSendBeamer.setActivated(true);
                }
                prefStorage.saveTermsIsAccepted(chbTerms.isActivated());
                break;
            case R.id.btn_send_beamer:
                if (prefStorage.isTermsAccepted()) {
                    presenter.sendMarkerByEmail(evEmail.getText().toString());
                }
                break;
            case R.id.tv_terms_link:
                final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(getString(R.string.link_browse_terms_condition)));
                startActivityForResult(intent, REQUEST_CODE_TERMS);
                break;
            case R.id.tv_load_marker_link:
//                try {
//                    showProgress();
//                    onSendBeamer(getAssets().open("beamer.pdf"), "beamer.pdf");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != REQUEST_CODE_PURCHASE && requestCode != REQUEST_CODE_TERMS && requestCode == REQUEST_CODE) {
            openBeamScreen(artPiece);
        }
    }

//    private void onSendBeamer(InputStream assetStream, String filename) {
//        Single.create(new SaveFileSubscriber(assetStream, filename))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new DisposableSingleObserver<String>() {
//                    @Override
//                    public void onSuccess(@io.reactivex.annotations.NonNull String s) {
//                        MediaScannerConnection.scanFile(SendBeamerActivity.this, new String[]{s},
//                                null, new MediaScannerConnection.MediaScannerConnectionClient() {
//                                    @Override
//                                    public void onMediaScannerConnected() {
//                                        //Nothing need here
//                                    }
//
//                                    @Override
//                                    public void onScanCompleted(String path, Uri uri) {
//                                        sendBeamer(uri);
//                                    }
//                                });
//
//                    }
//
//                    @Override
//                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
//                        showError(R.string.failed_share_beamer);
//                    }
//                });
//    }

//    private void sendBeamer(Uri path) {
//        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND, Uri.parse("mailto:"));
//        emailIntent.setType("text/plain");
//        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Beamer");
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{evEmail.getText().toString()});
//        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Here is the beamer ");
//        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
//        startActivityForResult(Intent.createChooser(emailIntent, "Send mail..."), REQUEST_CODE);
//        hideProgress();
//
//    }

    @Override
    public void showProgress() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flProgress.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    protected SendMarkerByEmailPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected SendMarkerByEmailPresenter.SendMarkerView getView() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.slide_screen_in_top, R.anim.slide_screen_out_top);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_screen_in_down, R.anim.slide_screen_out_down);
    }

    @Override
    public void showResult() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.success_send_marker)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openBeamScreen(artPiece);
                    }
                })
                .show();

    }

    @Override
    public void showErrorEmail(int error) {
        hideSoftKeyboard(evEmail);
        Snackbar.make(evEmail, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.fail_send_marker)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void openBeamScreen(final ArtPiece artPiece) {
        Intent intent = new Intent();
        intent.putExtra(BeamActivity.IMAGE, artPiece);
        setResult(RESULT_OK, intent);
        this.finish();
    }


//    private class SaveFileSubscriber implements SingleOnSubscribe<String> {
//
//        private final InputStream inputStream;
//        private final String filename;
//
//        SaveFileSubscriber(InputStream inputStream, String filename) {
//            this.inputStream = inputStream;
//            this.filename = filename;
//        }
//
//        @Override
//        public void subscribe(@io.reactivex.annotations.NonNull SingleEmitter<String> e) {
//            try {
//                e.onSuccess(FileUtils.saveIO(inputStream, filename));
//            } catch (Exception ex) {
//                e.onError(ex);
//            }
//        }
//
//    }
}
