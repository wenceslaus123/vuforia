package com.vuforia.vuforia.presentation.presenter;


import android.os.Handler;
import android.support.annotation.NonNull;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.token.TokenTask;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SplashPresenter extends Presenter<SplashPresenter.SplashView> {

    private long loadStarted;

    private final TokenTask tokenTask;

    @Inject
    SplashPresenter(TokenTask tokenTask) {

        this.tokenTask = tokenTask;
    }

    @Override
    protected void onAttachView(@NonNull SplashView view) {
        super.onAttachView(view);
        loadStarted = System.currentTimeMillis();
        tokenTask.retriveTocken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Boolean aBoolean) {
                        if (aBoolean) {
                            if (System.currentTimeMillis() > loadStarted + 2 * 1000) {
                                if (getView() != null) {
                                    getView().goToApp();
                                }
                            } else {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (getView() != null) {
                                            getView().goToApp();
                                        }
                                    }
                                }, 1200);
                            }
                        } else {
                            if (getView() != null) {
                                getView().showError(R.string.failed_connect);
                            }
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        if (getView() != null) {
                            getView().showError(R.string.failed_connect);
                        }
                    }

                    @Override
                    public void onComplete() {
                        //nothing need
                    }
                });
    }

    public interface SplashView extends IView {

        void goToApp();
    }
}
