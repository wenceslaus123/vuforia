package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.data.model.responce.TokenResponse;

import io.reactivex.Observable;

public class TokenRepository {

    private final Api api;
    private final String deviceId;

    public TokenRepository(Api api, String deviceId) {
        this.api = api;
        this.deviceId = deviceId;
    }

    public Observable<TokenResponse> retriveToken() {
        return Observable.create(new NetworkRequestExecutor<>(api.getToken(deviceId)));
    }
}
