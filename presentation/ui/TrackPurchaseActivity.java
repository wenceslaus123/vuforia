package com.vuforia.vuforia.presentation.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.presentation.presenter.TrackPresenter;
import com.vuforia.vuforia.utils.DeviceUtils;
import com.vuforia.vuforia.utils.EditTextDrawableHandler;
import com.vuforia.vuforia.utils.KeyBoardUtils.KeyBoardVisibilityChange;

import javax.inject.Inject;

import static com.vuforia.vuforia.utils.KeyBoardUtils.glueKeyBoardWatcher;
import static io.fabric.sdk.android.services.common.CommonUtils.hideKeyboard;

public class TrackPurchaseActivity extends BaseActivity<TrackPresenter.TrackView, TrackPresenter>
        implements TrackPresenter.TrackView, View.OnClickListener, KeyBoardVisibilityChange {

    public static final String EXTRA_CODE = "extra__known_code";
    @Inject
    TrackPresenter presenter;
    private FrameLayout flProgress;
    private FrameLayout flTrack;
    private FrameLayout flFeedback;
    private VuforiaBar actionBar;
    private AppCompatEditText evSearch;
    private AppCompatEditText evFeedback;
    private ImageButton btnReceiveArt;
    private ImageButton btnSatisfied;
    private Button btnSendFeedback;
    private RatingBar experienceBar;
    private ImageView logo;
    private AppCompatTextView tvTrackingNumber;
    private AppCompatTextView tvArtName;
    private AppCompatTextView tvCompanyName;
    private AppCompatTextView tvPostedTime;
    private AppCompatTextView tvExpectedTime;
    private AppCompatTextView tvStatusTrack;
    private AppCompatTextView tvStatus;
    private AppCompatTextView tvStatusDesc;
    private View feedBackView;
    private View separator;
    private View receivedContainer;
    private final View.OnClickListener tooltipListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showError(R.string.track_purchase_tooltip);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_purchase);
        actionBar = (VuforiaBar) findViewById(R.id.ab_vuf);
        actionBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        actionBar.setActionListener(tooltipListener);
        actionBar.setTitle(getString(R.string.title_track_purchase));
        flTrack = (FrameLayout) findViewById(R.id.fl_track_ref);
        flFeedback = (FrameLayout) findViewById(R.id.fl_track_feedback);
        flProgress = (FrameLayout) findViewById(R.id.fl_progress);
        evSearch = (AppCompatEditText) findViewById(R.id.ev_shipping_number);
        btnSatisfied = (ImageButton) findViewById(R.id.btn_satisfied);
        experienceBar = (RatingBar) findViewById(R.id.rb_experience);
        experienceBar.setStepSize(20);
        experienceBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                btnSendFeedback.setActivated((evFeedback.getText().length() > 0));
            }
        });
        evFeedback = (AppCompatEditText) findViewById(R.id.ev_feedback);
        evSearch.setOnTouchListener(new EditTextDrawableHandler(evSearch, new EditTextDrawableHandler.ButtonPressListener() {
            @Override
            public void onButtonPressed() {
                trackPurchase(evSearch.getText().toString());
            }
        }));
        evSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    trackPurchase(evSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        btnReceiveArt = (ImageButton) findViewById(R.id.btn_received_art);
        btnReceiveArt.setOnClickListener(this);
        btnReceiveArt.setActivated(false);
        logo = (ImageView) findViewById(R.id.ti_logo);
        tvTrackingNumber = (AppCompatTextView) findViewById(R.id.tv_tracking_number);
        tvArtName = (AppCompatTextView) findViewById(R.id.tv_art_name);
        tvCompanyName = (AppCompatTextView) findViewById(R.id.tv_shipping_company_name);
        tvPostedTime = (AppCompatTextView) findViewById(R.id.tv_posted_time);
        tvExpectedTime = (AppCompatTextView) findViewById(R.id.tv_expected_time);
        tvStatusTrack = (AppCompatTextView) findViewById(R.id.tv_status_track);
        tvStatus = (AppCompatTextView) findViewById(R.id.tfi_status);
        tvStatusDesc = (AppCompatTextView) findViewById(R.id.tfi_status_desc);
        feedBackView = findViewById(R.id.i_feedback_action);
        btnSendFeedback = (Button) findViewById(R.id.btn_post_feedback);
        receivedContainer = findViewById(R.id.tfi_received_container);
        separator = findViewById(R.id.fi_separator);
        glueKeyBoardWatcher(findViewById(R.id.root), this);
        VuforiaApplication.getComponent(this).inject(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() != null && getIntent().hasExtra(EXTRA_CODE)) {
            evSearch.setText(getIntent().getStringExtra(EXTRA_CODE));
            evSearch.setSelection(evSearch.getText().length());
        }
    }

    public void trackPurchase(String code) {
        hideKeyboard(this, evSearch);
        if (TextUtils.isEmpty(code)) {
            getView().showError(R.string.tracking_number_empty);
        } else {
            presenter.track(code);
        }
    }

    @Override
    public void showProgress() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flProgress.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    protected TrackPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected TrackPresenter.TrackView getView() {
        return this;
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(EXTRA_CODE)) {
            finish();
        } else if (flFeedback.getVisibility() == View.VISIBLE) {
            resetFeedback();
            flFeedback.setVisibility(View.GONE);
            flTrack.setVisibility(View.VISIBLE);
            evSearch.setEnabled(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_received_art:
                btnReceiveArt.setActivated(!btnReceiveArt.isActivated());
                feedBackView.setVisibility(btnReceiveArt.isActivated() ? View.VISIBLE : View.GONE);
                break;
            case R.id.btn_satisfied:
                btnSatisfied.setActivated(!btnSatisfied.isActivated());
                break;
            case R.id.btn_post_feedback:
                DeviceUtils.hideSoftKeyboard(evFeedback);
                String comment = evFeedback.getText().toString();
                int experienceBarRating = (int) experienceBar.getRating();
                presenter.postFeedback(btnSatisfied.isActivated(), comment, experienceBarRating);
                break;
            default:
                break;
        }
    }

    private void resetFeedback() {
        btnSatisfied.setActivated(false);
        btnSendFeedback.setActivated(false);
        experienceBar.setRating(0.0f);
        btnReceiveArt.setActivated(false);
        feedBackView.setVisibility(View.GONE);
        btnSatisfied.clearAnimation();
        experienceBar.clearAnimation();
        evFeedback.setText("");
        actionBar.setAction(R.mipmap.top_bar_icon_info);
        actionBar.setActionListener(tooltipListener);
    }

    @Override
    public void showErrorInfo() {
        evSearch.setHint(R.string.hint_not_valid_number_purchase);
        tvStatusTrack.setText(R.string.no_results_ref_postal_number);
    }

    @Override
    public void setTrackingNumber(String number) {
        tvTrackingNumber.setText(number);
    }

    @Override
    public void setArtName(String name) {
        tvArtName.setText(name);
    }

    @Override
    public void setCompanyName(String name) {
        tvCompanyName.setText(name);
    }

    @Override
    public void setPostDate(String date) {
        tvPostedTime.setText(date);
    }

    @Override
    public void setExpectedTime(String date) {
        tvExpectedTime.setText(date);
    }

    @Override
    public void switchFeedbackScreen() {
        evSearch.setHint(R.string.hint_number_purchase_input);
        tvStatusTrack.setText(R.string.label_enter_ref_postal_number);
        evSearch.setText("");
        evSearch.setEnabled(false);
        if (flTrack.getVisibility() == View.VISIBLE) {
            flTrack.setVisibility(View.GONE);
            actionBar.hideAction();
            flFeedback.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showStatus(String status) {
        receivedContainer.setVisibility(View.GONE);
        separator.setVisibility(View.GONE);
        tvStatusDesc.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(status);
    }

    @Override
    public void showFeedbackStatus() {
        int idMessage = R.string.negative_feedback_post;
        if (btnSatisfied.isActivated()) {
            idMessage = R.string.success_feedback_post;
        }
        resetFeedback();
        new AlertDialog.Builder(this)
                .setMessage(idMessage)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                })
                .show();
    }

    @Override
    public void keyBoardVisibilityChange(boolean visible) {
        logo.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
    }
}