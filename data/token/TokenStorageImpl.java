package com.vuforia.vuforia.data.token;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class TokenStorageImpl implements TokenStorage {

    private static final String COOKIES_KEY = "Cookies";

    private final SharedPreferences defaultSharedPreferences;

    public TokenStorageImpl(Context context) {
        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public String getCookies() {
        return defaultSharedPreferences.getString(COOKIES_KEY, "");
    }

    @Override
    public void setCookies(String cookies) {
        defaultSharedPreferences.edit().putString(COOKIES_KEY, cookies).apply();
    }

    @Override
    public void clearCookies() {
        defaultSharedPreferences.edit().remove(COOKIES_KEY).apply();
    }
}
