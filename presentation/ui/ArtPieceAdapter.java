package com.vuforia.vuforia.presentation.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.bumptech.glide.Glide;

import java.util.List;

import jp.wasabeef.blurry.Blurry;

import static com.vuforia.vuforia.presentation.ui.ArtPieceAdapter.ItemClickListener.TYPE_BEAM;
import static com.vuforia.vuforia.presentation.ui.ArtPieceAdapter.ItemClickListener.TYPE_BUY;

class ArtPieceAdapter extends RecyclerView.Adapter<ArtPieceAdapter.ArtPieceViewHolder> {

    public static final String X_PROPERTY = "x";
    public static final String ALPHA_PROPERTY = "alpha";

    private final List<ArtPiece> data;
    private ItemClickListener itemClickListener;
    private boolean isAnimationInAction;
    private int selectedPosition = -1;
    private int previosSelectedPosition = -1;

    ArtPieceAdapter(@NonNull List<ArtPiece> data) {
        this.data = data;
    }

    @Override
    public ArtPieceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArtPieceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artpiece, parent, false));
    }

    @Override
    public void onBindViewHolder(final ArtPieceViewHolder holder, int position) {
        ArtPiece artPiece = data.get(position);

        holder.nameExpand.setText(artPiece.getName());
        holder.parent.setTag(holder);
        holder.parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAnimationInAction) {
                    return;
                }
                animateItem((ArtPieceViewHolder) v.getTag());
            }
        });
        holder.beam.setTag(artPiece);
        holder.beam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null && !isAnimationInAction) {
                    itemClickListener.onItemClicked((ArtPiece) v.getTag(), TYPE_BEAM);
                }
            }
        });

        holder.buy.setTag(artPiece);
        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null && !isAnimationInAction) {
                    itemClickListener.onItemClicked((ArtPiece) v.getTag(), TYPE_BUY);
                }
            }
        });

        String currency = getCurrencySymbol(artPiece.getCurrency());
        float cost = (float) artPiece.getPrice();
        String priceArt = String.format(holder.parent.getContext().getString(R.string.format_art_price), currency, cost);
        if (priceArt != null) {
            holder.priceExpand.setText(priceArt);
        }

        String sizeIndicator = artPiece.getSizeIndicator();
        if (TextUtils.isEmpty(sizeIndicator)) {
            holder.sizes.clearCheck();
        } else {
            switch (sizeIndicator) {
                case "S":
                    holder.sizes.check(R.id.rb_s);
                    break;
                case "M":
                    holder.sizes.check(R.id.rb_m);
                    break;
                case "L":
                    holder.sizes.check(R.id.rb_l);
                    break;
                case "XL":
                    holder.sizes.check(R.id.rb_xl);
                    break;
                default:
                    holder.sizes.clearCheck();
                    break;
            }
        }

        if (position == previosSelectedPosition) {
            previosSelectedPosition = -1;
        }
        if (position == selectedPosition) {
            selectedPosition = -1;
        }

        holder.actionContainer.setVisibility(View.GONE);
        holder.sizes.setVisibility(View.GONE);

        holder.logo.setTranslationX(0);
        holder.logo2.setTranslationX(0);
        holder.divider.setTranslationX(0);
        holder.nameExpand.setTranslationX(0);
        holder.priceExpand.setTranslationX(0);

        holder.nameExpand.setGravity(Gravity.CENTER);

        Glide.with(holder.image.getContext())
                .load(artPiece.getImageUrl())
                .into(holder.image);
    }


    public String getCurrencySymbol(String currency) {
        if ("USD".equalsIgnoreCase(currency) || "AUD".equalsIgnoreCase(currency)) {
            return "$";
        } else if ("EUR".equalsIgnoreCase(currency)) {
            return "€";
        } else if ("GBP".equalsIgnoreCase(currency)) {
            return "£";
        } else {
            return currency;
        }
    }


    private void animateItem(final ArtPieceViewHolder holder) {

        isAnimationInAction = true;

        AnimatorSet set = new AnimatorSet();
        set.setDuration(300);

        if (holder.getAdapterPosition() == selectedPosition || holder.getAdapterPosition() == previosSelectedPosition) {
            selectedPosition = -1;
            set.play(ObjectAnimator.ofFloat(holder.logo, X_PROPERTY, holder.logo.getTranslationX() * -1));
            set.play(ObjectAnimator.ofFloat(holder.logo2, X_PROPERTY, holder.logo2.getTranslationX() * -1));
            set.play(ObjectAnimator.ofFloat(holder.divider, X_PROPERTY, holder.divider.getTranslationX() * -1));
            set.play(ObjectAnimator.ofFloat(holder.nameExpand, X_PROPERTY, holder.nameExpand.getTranslationX() * -1));
            set.play(ObjectAnimator.ofFloat(holder.priceExpand, X_PROPERTY, holder.priceExpand.getTranslationX() * -1));

            AnimatorListenerAdapter goneContainersListener = new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    holder.actionContainer.setVisibility(View.GONE);
                    holder.sizes.setVisibility(View.GONE);
                }
            };

            set.play(ObjectAnimator.ofFloat(holder.sizes, ALPHA_PROPERTY, 0));
            ObjectAnimator actionAnimator = ObjectAnimator.ofFloat(holder.actionContainer, ALPHA_PROPERTY, 0);
            actionAnimator.addListener(goneContainersListener);
            set.play(actionAnimator);

            holder.nameExpand.setGravity(Gravity.CENTER);

            Object tag = holder.actionContainer.getTag();
            if (tag != null) {
                holder.image.setImageDrawable((Drawable) tag);
            }
        } else {
            previosSelectedPosition = selectedPosition;
            selectedPosition = holder.getAdapterPosition();

            set.play(ObjectAnimator.ofFloat(holder.logo, X_PROPERTY, 0));
            set.play(ObjectAnimator.ofFloat(holder.logo2, X_PROPERTY, 0));
            set.play(ObjectAnimator.ofFloat(holder.divider, X_PROPERTY, 0));
            set.play(ObjectAnimator.ofFloat(holder.priceExpand, X_PROPERTY, 0));
            set.play(ObjectAnimator.ofFloat(holder.nameExpand, X_PROPERTY, 0));

            holder.actionContainer.setAlpha(0);
            holder.sizes.setAlpha(0);

            holder.actionContainer.setVisibility(View.VISIBLE);
            holder.sizes.setVisibility(View.VISIBLE);

            set.play(ObjectAnimator.ofFloat(holder.actionContainer, ALPHA_PROPERTY, 1));
            set.play(ObjectAnimator.ofFloat(holder.sizes, ALPHA_PROPERTY, 1));

            holder.actionContainer.setTag(holder.image.getDrawable());
            Blurry.with(holder.parent.getContext()).animate().capture(holder.image).into(holder.image);

            holder.nameExpand.setGravity(Gravity.START);
        }

        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimationInAction = false;
                if (previosSelectedPosition != -1) {
                    notifyItemChanged(previosSelectedPosition);
                }
            }
        });
        set.start();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void updateData(@NonNull List<ArtPiece> data) {
        int lastIndex = this.data.size() - 1;
        this.data.addAll(data);
        notifyItemRangeInserted(lastIndex, data.size());
    }

    public void clear() {
        data.clear();
        selectedPosition = -1;
        previosSelectedPosition = -1;
    }

    public void setData(@NonNull List<ArtPiece> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    interface ItemClickListener {
        int TYPE_BEAM = 0;
        int TYPE_BUY = 1;

        void onItemClicked(ArtPiece artPiece, int position);
    }

    static class ArtPieceViewHolder extends RecyclerView.ViewHolder {

        final View parent;
        ImageView image;
        TextView nameExpand;
        TextView priceExpand;
        ImageButton buy;
        ImageButton beam;
        RadioGroup sizes;
        LinearLayout infoContainer;
        LinearLayout actionContainer;
        TextView logo;
        TextView logo2;
        View divider;

        ArtPieceViewHolder(View parent) {
            super(parent);
            this.parent = parent;
            image = (ImageView) parent.findViewById(R.id.iv_preview);
            nameExpand = (TextView) parent.findViewById(R.id.tv_name_expand);
            priceExpand = (TextView) parent.findViewById(R.id.tv_price_expand);
            beam = (ImageButton) parent.findViewById(R.id.btn_beam);
            buy = (ImageButton) parent.findViewById(R.id.btn_buy);
            sizes = (RadioGroup) parent.findViewById(R.id.rg_sizes);
            infoContainer = (LinearLayout) parent.findViewById(R.id.ll_info);
            actionContainer = (LinearLayout) parent.findViewById(R.id.ll_action_art);
            logo = (TextView) parent.findViewById(R.id.tv_art_logo1);
            logo2 = (TextView) parent.findViewById(R.id.tv_art_logo2);
            divider = parent.findViewById(R.id.divider);
        }
    }
}
