package com.vuforia.vuforia.presentation.ui.beam;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import com.vuforia.Device;
import com.vuforia.Matrix44F;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vuforia;

import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PictureRenderer implements GLSurfaceView.Renderer, ARRendererControl {

    private final ARSession vuforiaAppSession;
    private final ARRenderer mARRenderer;

    private Texture mTexture;
    private long realWidth;
    private long realHeight;

    private int shaderProgramID;
    private int vertexHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;
    private int texSampler2DHandle;
    private MeshObject picture;

    private boolean mIsActive = false;
    private boolean mModelIsLoaded = false;

    private static final float OBJECT_SCALE_FLOAT = 0.1f;
    private int mViewWidth;
    private int mViewHeight;

    private boolean isNeedToSaveFrame = false;
    private PictureListener listener;

    PictureRenderer(Point point, ARSession session) {
        vuforiaAppSession = session;
        mARRenderer = new ARRenderer(this, point, Device.MODE.MODE_AR, false, 0.01f, 5f);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if (!mIsActive) {
            return;
        }
        mARRenderer.render();
        if (isNeedToSaveFrame) {
            isNeedToSaveFrame = false;
            saveScreenShot(0, 0, mViewWidth, mViewHeight);
        }
    }

    public void takePicture(PictureListener listener) {
        this.listener = listener;
        isNeedToSaveFrame = true;
    }

    public interface PictureListener {
        void pictureCreated(Bitmap picture);

        void error();
    }

    void setActive(boolean active) {
        mIsActive = active;

        if (mIsActive) {
            mARRenderer.configureVideoBackground();
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Timber.d("GLRenderer.onSurfaceCreated");
        vuforiaAppSession.onSurfaceCreated();
        mARRenderer.onSurfaceCreated();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Timber.d("GLRenderer.onSurfaceChanged");
        vuforiaAppSession.onSurfaceChanged(width, height);
        mARRenderer.onConfigurationChanged(mIsActive);
        mViewWidth = width;
        mViewHeight = height;
        if (mTexture != null) {
            initRendering();
        }
    }

    private void initRendering() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f : 1.0f);

        GLES20.glGenTextures(1, mTexture.mTextureID, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture.mTextureID[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mTexture.mWidth, mTexture.mHeight,
                0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mTexture.mData);

        shaderProgramID = ARUtils.createProgramFromShaderSrc(CubeShaders.CUBE_MESH_VERTEX_SHADER, CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexPosition");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID, "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID, "texSampler2D");

        if (!mModelIsLoaded) {
            picture = new Picture(realWidth, realHeight);
            mModelIsLoaded = true;
        }
    }

    public void renderFrame(State state, float... projectionMatrix) {
        mARRenderer.renderVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);

        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();
            printUserData(trackable);

            Matrix44F modelViewMatrixVuforia = Tool.convertPose2GLMatrix(result.getPose());

            float[] modelViewMatrix = modelViewMatrixVuforia.getData();
            float[] modelViewProjection = new float[16];

            Matrix.scaleM(modelViewMatrix, 0, OBJECT_SCALE_FLOAT, OBJECT_SCALE_FLOAT, OBJECT_SCALE_FLOAT);
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrix, 0);

            GLES20.glUseProgram(shaderProgramID);
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false, 0, picture.getVertices());
            GLES20.glVertexAttribPointer(textureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, picture.getTexCoords());
            GLES20.glEnableVertexAttribArray(vertexHandle);
            GLES20.glEnableVertexAttribArray(textureCoordHandle);
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture.mTextureID[0]);
            GLES20.glUniform1i(texSampler2DHandle, 0);
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, modelViewProjection, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, picture.getNumObjectIndex(), GLES20.GL_UNSIGNED_SHORT, picture.getIndices());
            GLES20.glDisableVertexAttribArray(vertexHandle);
            GLES20.glDisableVertexAttribArray(textureCoordHandle);

            ARUtils.checkGLError("Render Frame");
        }

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
    }

    private void printUserData(Trackable trackable) {
        String userData = (String) trackable.getUserData();
        Timber.d("UserData:Retreived User Data\"%s\"", userData);
    }


    public void setTextures(Texture texture, long realWidth, long realHeight) {
        mTexture = texture;

        this.realWidth = realWidth;
        this.realHeight = realHeight;
    }

    private void saveScreenShot(final int x, final int y, final int w, final int h) {
        final int b[] = new int[w * (y + h)];
        final int bt[] = new int[w * h];

        IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);
        GLES20.glReadPixels(x, 0, w, y + h,
                GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);

        Single.create(new SaveFileSubscriber(w, h, b, bt))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<Bitmap>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        //Nothing need here yet
                    }

                    @Override
                    public void onSuccess(@NonNull Bitmap s) {
                        if (listener != null) {
                            listener.pictureCreated(s);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Timber.e(e);
                        if (listener != null) {
                            listener.error();
                        }
                    }
                });

    }

    private class SaveFileSubscriber implements SingleOnSubscribe<Bitmap> {

        private final int w;
        private final int h;
        private final int[] b;
        private final int[] bt;

        SaveFileSubscriber(final int w, final int h, int[] b, int... bt) {
            this.w = w;
            this.h = h;
            this.b = b;
            this.bt = bt;
        }

        @Override
        public void subscribe(@NonNull SingleEmitter<Bitmap> e) {
            try {
                for (int i = 0, k = 0; i < h; i++, k++) {
                    for (int j = 0; j < w; j++) {
                        int pix = b[i * w + j];
                        int pb = (pix >> 16) & 0xff;
                        int pr = (pix << 16) & 0x00ff0000;
                        int pix1 = (pix & 0xff00ff00) | pr | pb;
                        bt[(h - k - 1) * w + j] = pix1;
                    }
                }

                Bitmap bitmap = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
                e.onSuccess(bitmap/*FileUtils.saveBitmapToInternalStorageGallery(bitmap, filename, "")*/);
            } catch (Exception ex) {
                e.onError(ex);
            }
        }
    }
}
