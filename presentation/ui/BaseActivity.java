package com.vuforia.vuforia.presentation.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.presentation.presenter.IView;
import com.vuforia.vuforia.presentation.presenter.Presenter;


public abstract class BaseActivity<V extends IView, P extends Presenter<V>> extends AppCompatActivity implements IView {

    @NonNull
    protected abstract P getPresenter();

    @NonNull
    protected abstract V getView();

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().attachView(getView());
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().detachView();
    }

    @NonNull
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(@StringRes int error) {
        showError(getString(error));
    }

    @Override
    public void showError(String error) {
        Snackbar.make(findViewById(R.id.root), error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(CallServerException ex) {
        showError(ex.getUserFriendlyErrorText(getResources()));
    }
}
