package com.vuforia.vuforia.data.model.responce;

import com.vuforia.vuforia.domain.model.PurchaseDetailModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchaseDetailsResponse {

    @SerializedName("id")
    @Expose
    public long id;

    @SerializedName("trackingNumber")
    @Expose
    public String trackingNumber;

    @SerializedName("nameOfArt")
    @Expose
    public String nameOfArt;

    @SerializedName("shippingCompany")
    @Expose
    public String shippingCompany;

    @SerializedName("postDate")
    @Expose
    public String postDate;

    @SerializedName("expectedArrivalTime")
    @Expose
    public String expectedArrivalTime;

    @SerializedName("status")
    @Expose
    public String status;

    @Override
    public String toString() {
        return "ArtpieceItem{" +
                "id=" + id +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", nameOfArt=" + nameOfArt +
                ", shippingCompany=" + shippingCompany +
                ", postDate='" + postDate + '\'' +
                ", expectedArrivalTime='" + expectedArrivalTime + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public PurchaseDetailModel convertToPurchaseModel() {
        PurchaseDetailModel result = PurchaseDetailModel.builder()
                .id(id)
                .trackingNumber(trackingNumber)
                .nameOfArt(nameOfArt)
                .shippingCompany(shippingCompany)
                .postDate(postDate)
                .expectedArrivalTime(expectedArrivalTime)
                .status(status)
                .build();
        return result;
    }

}

