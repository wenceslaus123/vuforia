package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.data.repository.CountriesRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CountryUseCase extends UseCase<List<Country>, Void> {

    private final CountriesRepository repository;

    @Inject
    CountryUseCase(CountriesRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<List<Country>> buildUseCaseObservable(Void v) {
        return repository.getCountries();
    }
}
