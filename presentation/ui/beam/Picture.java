package com.vuforia.vuforia.presentation.ui.beam;

import java.nio.Buffer;

public class Picture extends MeshObject {

    private static final float planeTexcoords[] = {
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f};

    private static final float planeNormals[] = {
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f};

    private static final short planeIndices[] = {0, 1, 2, 0, 2, 3};
    private static final float HALF_A4_WIDTH = 0.0895f;
    private static final float MM_MULTIPILLER = 0.001f;

    private final Buffer verts;
    private final Buffer textCoords;
    private final Buffer norms;
    private final Buffer indices;


    Picture(long width, long height) {
        float x = ((width * MM_MULTIPILLER) / HALF_A4_WIDTH) / 2;
        float y = ((height * MM_MULTIPILLER) / HALF_A4_WIDTH) / 2;

        float[] planeVertices = {
                -1 * x, -1 * y, 0.0f,
                x, -1 * y, 0.0f,
                x, y, 0.0f,
                -1 * x, y, 0.0f};

        verts = fillBuffer(planeVertices);
        textCoords = fillBuffer(planeTexcoords);
        norms = fillBuffer(planeNormals);
        indices = fillBuffer(planeIndices);
    }

    @Override
    public Buffer getBuffer(BufferType bufferType) {
        Buffer result = null;
        switch (bufferType) {
            case BUFFER_TYPE_VERTEX:
                result = verts;
                break;
            case BUFFER_TYPE_TEXTURE_COORD:
                result = textCoords;
                break;
            case BUFFER_TYPE_INDICES:
                result = indices;
                break;
            case BUFFER_TYPE_NORMALS:
                result = norms;
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    public int getNumObjectIndex() {
        return planeIndices.length;
    }
}
