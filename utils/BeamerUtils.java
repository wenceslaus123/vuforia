package com.vuforia.vuforia.utils;

import android.content.SharedPreferences;

public final class BeamerUtils {

    private static final String NEED_SHOW_SEND_BEAMER_SCREEN_KEY = "need_show_send_beamer_screen_key";

    private BeamerUtils() {
        //Utils class
    }

    public static boolean isNeedGoToSendBEamerScreen(SharedPreferences sharedPreferences) {
        if (sharedPreferences.contains(NEED_SHOW_SEND_BEAMER_SCREEN_KEY)) {
            return sharedPreferences.getBoolean(NEED_SHOW_SEND_BEAMER_SCREEN_KEY, false);
        } else {
            sharedPreferences.edit()
                    .putBoolean(NEED_SHOW_SEND_BEAMER_SCREEN_KEY, false)
                    .apply();
            return true;
        }
    }
}
