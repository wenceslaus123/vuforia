package com.vuforia.vuforia.dagger;

import android.content.Context;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.data.NetworkAdapter;
import com.vuforia.vuforia.data.repository.CountriesRepository;
import com.vuforia.vuforia.data.repository.FeedbackRepositoryImpl;
import com.vuforia.vuforia.data.repository.PicturesRepositoryImpl;
import com.vuforia.vuforia.data.repository.PurchaseDetailsRepositoryImp;
import com.vuforia.vuforia.data.repository.SaleRepository;
import com.vuforia.vuforia.data.repository.SendMarkerByEmailByEmailRepositoryImpl;
import com.vuforia.vuforia.data.repository.TokenRepository;
import com.vuforia.vuforia.data.token.TokenStorage;
import com.vuforia.vuforia.data.token.TokenStorageImpl;
import com.vuforia.vuforia.domain.repository.FeedbackRepository;
import com.vuforia.vuforia.domain.repository.PicturesRepository;
import com.vuforia.vuforia.domain.repository.PurchaseDetailsRepository;
import com.vuforia.vuforia.domain.repository.SendMarkerByEmailRepository;
import com.vuforia.vuforia.utils.DeviceUtils;
import com.vuforia.vuforia.utils.PrefStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    private final Context applicationContext;

    public DataModule(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Singleton
    @Provides
    public NetworkAdapter provideNetworkAdapter() {
        return new NetworkAdapter(provideCookiesStorage());
    }

    @Singleton
    @Provides
    public TokenStorage provideCookiesStorage() {
        return new TokenStorageImpl(applicationContext);
    }

    @Singleton
    @Provides
    public Api provideApi() {
        return provideNetworkAdapter().makeClient(Api.class);
    }

    @Singleton
    @Provides
    public TokenRepository provideTokenRepository() {
        return new TokenRepository(provideApi(), DeviceUtils.generateUniqueID(applicationContext));
    }

    @Provides
    public PurchaseDetailsRepository provideDetailsRepository() {
        return new PurchaseDetailsRepositoryImp(provideApi());
    }

    @Provides
    public FeedbackRepository provideFeedbackRepository() {
        return new FeedbackRepositoryImpl(provideApi());
    }

    @Provides
    public SendMarkerByEmailRepository provideSendMarkerRepository() {
        return new SendMarkerByEmailByEmailRepositoryImpl(provideApi());
    }

    @Provides
    public PicturesRepository provideRepository() {
        return new PicturesRepositoryImpl(provideApi());
    }

    @Provides
    public CountriesRepository provideCountryRepository() {
        return new CountriesRepository(provideApi());
    }

    @Provides
    public SaleRepository provideSaleRepository() {
        return new SaleRepository(provideApi());
    }

    @Provides
    public PrefStorage providePrefStorage() {
        return new PrefStorage(applicationContext);
    }

}
