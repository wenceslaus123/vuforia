package com.vuforia.vuforia.domain.repository;

import com.vuforia.vuforia.domain.model.PurchaseDetailModel;

import io.reactivex.Observable;

public interface PurchaseDetailsRepository {

    Observable<PurchaseDetailModel> track(String searchQuery);
}
