package com.vuforia.vuforia.data.token;

import android.text.TextUtils;

import com.vuforia.vuforia.data.model.responce.TokenResponse;
import com.vuforia.vuforia.data.repository.TokenRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class TokenTask {

    private final TokenStorage tokenStorage;
    private final TokenRepository tokenRepository;

    @Inject
    TokenTask(TokenStorage tokenStorage, TokenRepository tokenRepository) {
        this.tokenStorage = tokenStorage;
        this.tokenRepository = tokenRepository;
    }

    public Observable<Boolean> retriveTocken() {
        return tokenRepository.retriveToken()
                .map(new Function<TokenResponse, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull TokenResponse tokenResponse) throws Exception {
                        if (TextUtils.isEmpty(tokenResponse.getToken())) {
                            return false;
                        } else {
                            tokenStorage.setCookies(tokenResponse.getToken());
                            return true;
                        }
                    }
                });
    }

}
