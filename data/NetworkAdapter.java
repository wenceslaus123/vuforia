package com.vuforia.vuforia.data;

import android.support.annotation.NonNull;

import com.vuforia.vuforia.BuildConfig;
import com.vuforia.vuforia.data.token.TokenStorage;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkAdapter {

    public static final String AUTHORIZATION = "Authorization";
    private final Retrofit restAdapter;
    private final TokenStorage cookiesStorage;

    public NetworkAdapter(TokenStorage cookiesStorage) {
        this.cookiesStorage = cookiesStorage;
        restAdapter = new Retrofit.Builder()
                .client(new OkHttpClient.Builder()
                        .addInterceptor(getRequestInterceptor())
                        .addInterceptor(new HttpLoggingInterceptor()
                                .setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build())
                .baseUrl(BuildConfig.API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    @NonNull
    private Interceptor getRequestInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder requestBuilder = request.newBuilder()
                        .addHeader(AUTHORIZATION, String.format(Locale.US, "Bearer %s", cookiesStorage.getCookies()));

                Request requestNew = requestBuilder.build();
                return chain.proceed(requestNew);
            }
        };
    }

    public <T> T makeClient(Class<T> clientClass) {
        return restAdapter.create(clientClass);
    }
}