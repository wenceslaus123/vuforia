package com.vuforia.vuforia.data.repository;


import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.domain.model.MarkerModel;
import com.vuforia.vuforia.domain.repository.SendMarkerByEmailRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SendMarkerByEmailByEmailRepositoryImpl implements SendMarkerByEmailRepository {

    private final Api api;

    @Inject
    public SendMarkerByEmailByEmailRepositoryImpl(Api api) {
        this.api = api;
    }

    @Override
    public Observable<Void> sendMarkerByEmail(MarkerModel model) {
        return Observable.create(new NetworkRequestExecutor<>(api.sendMarkerByEmail(model)));
    }
}
