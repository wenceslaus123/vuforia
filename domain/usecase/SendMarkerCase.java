package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.domain.model.MarkerModel;
import com.vuforia.vuforia.domain.repository.SendMarkerByEmailRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SendMarkerCase extends UseCase<Void, MarkerModel> {

    private final SendMarkerByEmailRepository repository;

    @Inject
    SendMarkerCase(SendMarkerByEmailRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(MarkerModel m) {
        return repository.sendMarkerByEmail(m);
    }
}
