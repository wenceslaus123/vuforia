package com.vuforia.vuforia.domain.model;

import android.os.Parcel;
import android.os.Parcelable;


public class PurchaseDetailModel implements Parcelable {

    public static final Creator<PurchaseDetailModel> CREATOR = new Creator<PurchaseDetailModel>() {
        @Override
        public PurchaseDetailModel createFromParcel(Parcel in) {
            return new PurchaseDetailModel(in);
        }

        @Override
        public PurchaseDetailModel[] newArray(int size) {
            return new PurchaseDetailModel[size];
        }
    };
    private final long id;
    private final String trackingNumber;
    private final String nameOfArt;
    private final String shippingCompany;
    private final String postDate;
    private final String expectedArrivalTime;
    private final String status;

    PurchaseDetailModel(Builder builder) {
        id = builder.id;
        trackingNumber = builder.trackingNumber;
        nameOfArt = builder.nameOfArt;
        shippingCompany = builder.shippingCompany;
        postDate = builder.postDate;
        expectedArrivalTime = builder.expectedArrivalTime;
        status = builder.status;
    }

    protected PurchaseDetailModel(Parcel in) {
        id = in.readLong();
        trackingNumber = in.readString();
        nameOfArt = in.readString();
        shippingCompany = in.readString();
        postDate = in.readString();
        expectedArrivalTime = in.readString();
        status = in.readString();
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(trackingNumber);
        dest.writeString(nameOfArt);
        dest.writeString(shippingCompany);
        dest.writeString(postDate);
        dest.writeString(expectedArrivalTime);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "PurchaseModel{" +
                "id=" + id +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", nameOfArt=" + nameOfArt +
                ", shippingCompany=" + shippingCompany +
                ", postDate='" + postDate + '\'' +
                ", expectedArrivalTime='" + expectedArrivalTime + '\'' +
                ", status='" + status + '\'' +
                '}';
    }


    public long getId() {
        return id;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public String getNameOfArt() {
        return nameOfArt;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public String getPostDate() {
        return postDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public String getStatus() {
        return status;
    }

    public static final class Builder {
        private long id;
        private String trackingNumber;
        private String nameOfArt;
        private String shippingCompany;
        private String postDate;
        private String expectedArrivalTime;
        private String status;


        public Builder() {
            //Nothing here
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder trackingNumber(String val) {
            trackingNumber = val;
            return this;
        }

        public Builder nameOfArt(String val) {
            nameOfArt = val;
            return this;
        }

        public Builder shippingCompany(String val) {
            shippingCompany = val;
            return this;
        }

        public Builder postDate(String val) {
            postDate = val;
            return this;
        }

        public Builder expectedArrivalTime(String val) {
            expectedArrivalTime = val;
            return this;
        }

        public Builder status(String val) {
            status = val;
            return this;
        }

        public PurchaseDetailModel build() {
            return new PurchaseDetailModel(this);
        }

    }
}
