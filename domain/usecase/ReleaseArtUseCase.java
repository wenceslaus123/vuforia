package com.vuforia.vuforia.domain.usecase;

import com.vuforia.vuforia.data.repository.SaleRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ReleaseArtUseCase extends UseCase<Void, Long> {

    private final SaleRepository repository;

    @Inject
    ReleaseArtUseCase(SaleRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Long id) {
        return repository.releaseeArt(id);
    }
}
