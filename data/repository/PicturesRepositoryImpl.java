package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.data.model.responce.ArtpieceSearchResponse;
import com.vuforia.vuforia.domain.repository.PicturesRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class PicturesRepositoryImpl implements PicturesRepository {


    private final Api api;

    @Inject
    public PicturesRepositoryImpl(Api api) {
        this.api = api;
    }

    @Override
    public Observable<ArtpieceSearchResponse> search(String searchQuery, long page) {
        return Observable.create(new NetworkRequestExecutor<>(api.search(searchQuery, page, PAGE_SIZE)));
    }
}