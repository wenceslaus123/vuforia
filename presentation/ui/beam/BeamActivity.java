package com.vuforia.vuforia.presentation.ui.beam;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.R;
import com.vuforia.vuforia.domain.model.ArtPiece;
import com.vuforia.vuforia.presentation.presenter.BeamPresenter;
import com.vuforia.vuforia.presentation.ui.VuforiaBar;
import com.vuforia.vuforia.presentation.ui.BaseActivity;
import com.vuforia.vuforia.presentation.ui.CapturedPhotoActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

@SuppressWarnings("PMD")
public class BeamActivity extends BaseActivity<BeamPresenter.BeamView, BeamPresenter> implements BeamPresenter.BeamView, ARControl {


    public static final String IMAGE = "Image";
    ARSession vuforiaAppSession;

    @Inject
    BeamPresenter presenter;
    boolean mIsDroidDevice = false;
    private DataSet mCurrentDataset;
    private ArrayList<String> mDatasetStrings = new ArrayList<>();
    private ImageBeamGLView mGlView;
    private PictureRenderer mRenderer;
    private Texture mTextures;
    private boolean mSwitchDatasetAsap = false;
    private View mUILayout;
    private AlertDialog mErrorDiaTimber;
    private ArtPiece artPiece;
    private FrameLayout flProgress;
    private VuforiaBar vufBar;
    private View description;

    @NonNull
    @Override
    protected BeamPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    protected BeamPresenter.BeamView getView() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VuforiaApplication.getComponent(this).inject(this);
        vuforiaAppSession = new ARSession(this);
        startLoadingAnimation();
        mDatasetStrings.add("Target.xml");
        vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mIsDroidDevice = android.os.Build.MODEL.toLowerCase(Locale.US).startsWith("droid");

        artPiece = getIntent().getParcelableExtra(IMAGE);

        Glide.with(this)
                .load(artPiece.getImageUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        mTextures = Texture.convertBitmapToTexture(resource);
                        tryToResumeAR();
                    }
                });

        View cameraButton = findViewById(R.id.camera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        description = findViewById(R.id.co_description);

        findViewById(R.id.co_arrow_down).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getTag() == null) {
                    ((ImageView) v).setImageResource(R.mipmap.scr_photo_btn_detail_off);
                    description.setVisibility(View.GONE);
                    v.setTag(true);
                } else {
                    ((ImageView) v).setImageResource(R.mipmap.scr_photo_btn_detail_on);
                    description.setVisibility(View.VISIBLE);
                    v.setTag(null);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mIsDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        tryToResumeAR();

        if (mGlView != null) {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    private void tryToResumeAR() {
        if (mTextures != null) {
            try {
                vuforiaAppSession.resumeAR();
            } catch (ARException e) {
                Timber.e(e.getString());
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        vuforiaAppSession.onConfigurationChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGlView != null) {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        try {
            vuforiaAppSession.pauseAR();
        } catch (ARException e) {
            Timber.e(e.getString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            vuforiaAppSession.stopAR();
        } catch (ARException e) {
            Timber.e(e.getString());
        }

        mTextures = null;

        System.gc();
    }

    public void takePicture() {
        showProgress();
        mRenderer.takePicture(new PictureRenderer.PictureListener() {
            @Override
            public void pictureCreated(Bitmap photo) {
                presenter.savePhoto(photo);
            }

            @Override
            public void error() {
                hideProgress();
                showError(R.string.save_photo_failed);
            }
        });
    }

    @Override
    public void photoSaved(File photo) {
        Intent intent = new Intent(BeamActivity.this, CapturedPhotoActivity.class);
        intent.putExtra(CapturedPhotoActivity.CAPTURED_PHOTO_PATH, photo);
        startActivity(intent);
    }

    private void initApplicationAR() {
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new ImageBeamGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getRealSize(point);
        mRenderer = new PictureRenderer(point, vuforiaAppSession);
        mRenderer.setTextures(mTextures, artPiece.getBeamWidth(), artPiece.getBeamHeight());
        mGlView.setRenderer(mRenderer);
        findViewById(R.id.camera).setVisibility(View.VISIBLE);
    }

    private void startLoadingAnimation() {
        mUILayout = View.inflate(this, R.layout.camera_overlay, null);
        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.TRANSPARENT);
        vufBar = (VuforiaBar) mUILayout.findViewById(R.id.ab_vuf);
        vufBar.hideAction();
        flProgress = (FrameLayout) mUILayout.findViewById(R.id.fl_progress);

        addContentView(mUILayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        vufBar.setHomeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager.getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(mDatasetStrings.get(0), STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();

        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            String name = String.format("Current Dataset : %s", trackable.getName());
            trackable.setUserData(name);
            trackable.startExtendedTracking();
            Timber.d("UserData:Set the following user data %s", trackable.getUserData());
        }

        return true;
    }

    @Override
    public boolean doUnloadTrackersData() {
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager.getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive()) {
            if (objectTracker.getActiveDataSet(0).equals(mCurrentDataset) && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }

    @Override
    public void onInitARDone(ARException exception) {
        if (exception == null) {
            initApplicationAR();

            if (mTextures != null) {
                mRenderer.setActive(true);
            }

            addContentView(mGlView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            mUILayout.bringToFront();
            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            } catch (ARException e) {
                Timber.e(e.getString());
            }

            if (!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO)) {
                Timber.d("Failed to set FOCUS_MODE_CONTINUOUSAUTO");
            }
        } else {
            Timber.e(exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }

    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                if (mErrorDiaTimber != null) {
                    mErrorDiaTimber.dismiss();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(BeamActivity.this);
                builder.setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(getString(android.R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface diaTimber, int id) {
                                        finish();
                                    }
                                });

                mErrorDiaTimber = builder.create();
                mErrorDiaTimber.show();
            }
        });
    }

    @Override
    public void onVuforiaUpdate(State state) {
        if (mSwitchDatasetAsap) {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker.getClassType());
            if (ot == null || mCurrentDataset == null || ot.getActiveDataSet(0) == null) {
                Timber.d("Failed to swap datasets");
                return;
            }

            ot.resetExtendedTracking();

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }

    @Override
    public boolean doInitTrackers() {
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Timber.e("Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Timber.i("Tracker successfully initialized");
        }
        return result;
    }

    @Override
    public boolean doStartTrackers() {
        Tracker objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return true;
    }

    @Override
    public boolean doStopTrackers() {
        Tracker objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return true;
    }

    @Override
    public boolean doDeinitTrackers() {
        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());
        return true;
    }

    @Override
    public void showProgress() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flProgress.setVisibility(View.GONE);
    }
}
