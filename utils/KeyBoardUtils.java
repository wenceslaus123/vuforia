package com.vuforia.vuforia.utils;


import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

public final class KeyBoardUtils {

    public interface KeyBoardVisibilityChange {
        void keyBoardVisibilityChange(boolean visible);
    }

    private KeyBoardUtils() {
        //DO NOTHING
    }

    public static void glueKeyBoardWatcher(final View contentView, final KeyBoardVisibilityChange keyBoardVisibilityChange) {
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                // 0.15 ratio is perhaps enough to determine keypad height.
                keyBoardVisibilityChange.keyBoardVisibilityChange(keypadHeight > screenHeight * 0.15);
            }
        });
    }
}
