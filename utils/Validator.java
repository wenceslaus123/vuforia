package com.vuforia.vuforia.utils;

import android.text.TextUtils;

public final class Validator {
    private static final String EMAIL_VALIDATION_PATTERN = "\\b[+\\w.%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

    public static boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && email.matches(EMAIL_VALIDATION_PATTERN);
    }

    private Validator() {
        //DO NOTHING
    }
}
