package com.vuforia.vuforia.presentation.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.utils.FileUtils;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@SuppressWarnings("PMD")
public class BeamPresenter extends Presenter<BeamPresenter.BeamView> {

    @Inject
    public BeamPresenter() {
//Nothing need here yet
    }

    @Override
    protected void onAttachView(@NonNull BeamView view) {
//Nothing need here yet
    }

    @Override
    protected void onDetachView(@NonNull BeamView view) {
//Nothing need here yet
    }

    @Override
    protected void onDestroy() {
//Nothing need here yet
    }

    public void savePhoto(final Bitmap photo) {
        BeamView view = getView();
        if (view != null) {
            final File filesDir = view.getContext().getFilesDir();
            Single.just(photo)
                    .map(new Function<Bitmap, File>() {
                        @Override
                        public File apply(@io.reactivex.annotations.NonNull Bitmap bitmap) throws Exception {
                            return FileUtils.saveBitmapToInternalStorageGallery(bitmap, "photo" + System.currentTimeMillis() + ".png", filesDir);
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<File>() {
                        @Override
                        public void onSuccess(@io.reactivex.annotations.NonNull File photo) {
                            BeamView view = getView();
                            if (view != null) {
                                view.hideProgress();
                                view.photoSaved(photo);
                            }
                        }

                        @Override
                        public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                            BeamView view = getView();
                            if (view != null) {
                                view.hideProgress();
                                view.showError(R.string.save_photo_failed);
                            }

                        }
                    });
        }
    }

    public interface BeamView extends IView {
        void photoSaved(File photo);
    }
}