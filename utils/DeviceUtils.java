package com.vuforia.vuforia.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class DeviceUtils {

    private static final String HASH_STANDARD = "MD5";

    private DeviceUtils() {
        //Utils class
    }

    @SuppressLint("HardwareIds")
    public static String generateUniqueID(Context context) {
        String result = null;

        try {
            result = Settings.Secure.getString(context.getApplicationContext()
                    .getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(result)) {
            result = "355" + Build.BOARD.length() %
                    10 + Build.BRAND.length() %
                    10 + Build.CPU_ABI.length() %
                    10 + Build.DEVICE.length() %
                    10 + Build.DISPLAY.length() %
                    10 + Build.HOST.length() %
                    10 + Build.ID.length() %
                    10 + Build.MANUFACTURER.length() %
                    10 + Build.MODEL.length() %
                    10 + Build.PRODUCT.length() %
                    10 + Build.TAGS.length() %
                    10 + Build.TYPE.length() % 10 +
                    Build.USER.length() % 10;
        }

        result = result + Build.TIME;

        try {
            result = new BigInteger(1, MessageDigest.getInstance(HASH_STANDARD).digest(result.getBytes())).toString(16);

            while (result.length() < 32) {
                result = "0" + result;
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if ("000000000000000".equals(result)) {
            return "0123456789";
        }
        return result;
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Shows the soft keyboard
     */
    public static void showSoftKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }
}