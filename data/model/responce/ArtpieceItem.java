package com.vuforia.vuforia.data.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("PMD")
public class ArtpieceItem {

    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("beamHeight")
    @Expose
    public double beamHeight;
    @SerializedName("beamWidth")
    @Expose
    public double beamWidth;
    @SerializedName("height")
    @Expose
    public double height;
    @SerializedName("width")
    @Expose
    public double width;
    @SerializedName("measuringType")
    @Expose
    public String measuringType;
    @SerializedName("placeName")
    @Expose
    public String placeName;
    @SerializedName("price")
    @Expose
    public long price;
    @SerializedName("mediumType")
    @Expose
    public String mediumType;
    @SerializedName("isFramed")
    @Expose
    public boolean isFramed;
    @SerializedName("suitableRooms")
    @Expose
    public List<String> suitableRooms = null;
    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;
    @SerializedName("thumbnailUrl")
    @Expose
    public String thumbImageUrl;
    @SerializedName("artistName")
    @Expose
    public String artistName;
    @SerializedName("bio")
    @Expose
    public String bio;
    @SerializedName("avatarUrl")
    @Expose
    public String avatarUrl;
    @SerializedName("artistCountry")
    @Expose
    public String artistCountry;
    @SerializedName("sizeIndicator")
    @Expose
    public String sizeIndicator;
    @SerializedName("currency")
    @Expose
    public String currency;


    @Override
    public String toString() {
        return "ArtpieceItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", measuringType='" + measuringType + '\'' +
                ", placeName='" + placeName + '\'' +
                ", price=" + price +
                ", mediumType='" + mediumType + '\'' +
                ", isFramed=" + isFramed +
                ", suitableRooms=" + suitableRooms +
                ", imageUrl='" + imageUrl + '\'' +
                ", thumbImageUrl='" + thumbImageUrl + '\'' +
                ", artistName='" + artistName + '\'' +
                ", artistCountry='" + artistCountry + '\'' +
                ", sizeIndicator='" + sizeIndicator + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArtpieceItem that = (ArtpieceItem) o;

        if (id != that.id) return false;
        if (height != that.height) return false;
        if (width != that.width) return false;
        if (price != that.price) return false;
        if (isFramed != that.isFramed) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (measuringType != null ? !measuringType.equals(that.measuringType) : that.measuringType != null)
            return false;
        if (placeName != null ? !placeName.equals(that.placeName) : that.placeName != null)
            return false;
        if (mediumType != null ? !mediumType.equals(that.mediumType) : that.mediumType != null)
            return false;
        if (suitableRooms != null ? !suitableRooms.equals(that.suitableRooms) : that.suitableRooms != null)
            return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        if (thumbImageUrl != null ? !thumbImageUrl.equals(that.thumbImageUrl) : that.thumbImageUrl != null)
            return false;
        if (artistName != null ? !artistName.equals(that.artistName) : that.artistName != null)
            return false;
        if (artistCountry != null ? !artistCountry.equals(that.artistCountry) : that.artistCountry != null)
            return false;
        if (sizeIndicator != null ? !sizeIndicator.equals(that.sizeIndicator) : that.sizeIndicator != null)
            return false;
        return currency != null ? currency.equals(that.currency) : that.currency == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = (int) (31 * result + (height));
        result = (int) (31 * result + (width));
        result = 31 * result + (measuringType != null ? measuringType.hashCode() : 0);
        result = 31 * result + (placeName != null ? placeName.hashCode() : 0);
        result = 31 * result + (int) (price ^ (price >>> 32));
        result = 31 * result + (mediumType != null ? mediumType.hashCode() : 0);
        result = 31 * result + (isFramed ? 1 : 0);
        result = 31 * result + (suitableRooms != null ? suitableRooms.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (thumbImageUrl != null ? thumbImageUrl.hashCode() : 0);
        result = 31 * result + (artistName != null ? artistName.hashCode() : 0);
        result = 31 * result + (artistCountry != null ? artistCountry.hashCode() : 0);
        result = 31 * result + (sizeIndicator != null ? sizeIndicator.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        return result;
    }
}
