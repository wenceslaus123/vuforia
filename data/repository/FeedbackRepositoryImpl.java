package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.domain.model.FeedbackModel;
import com.vuforia.vuforia.domain.repository.FeedbackRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class FeedbackRepositoryImpl implements FeedbackRepository {
    private final Api api;

    @Inject
    public FeedbackRepositoryImpl(Api api) {
        this.api = api;
    }

    @Override
    public Observable<Object> postFeedback(FeedbackModel model) {
        return Observable.create(new NetworkRequestExecutor<>(api.postFeedback(model)));
    }
}
