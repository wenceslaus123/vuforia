package com.vuforia.vuforia.data;

import com.vuforia.vuforia.data.model.responce.ArtpieceSearchResponse;
import com.vuforia.vuforia.data.model.responce.BaseResponse;
import com.vuforia.vuforia.data.model.responce.Charge;
import com.vuforia.vuforia.data.model.responce.Country;
import com.vuforia.vuforia.data.model.responce.PurchaseDetailsResponse;
import com.vuforia.vuforia.data.model.responce.TokenResponse;
import com.vuforia.vuforia.domain.model.FeedbackModel;
import com.vuforia.vuforia.domain.model.MarkerModel;
import com.vuforia.vuforia.domain.model.Sale;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("/api/artpiece")
    Call<BaseResponse<ArtpieceSearchResponse>> search(@Query("searchQuery") String query, @Query("pageNumber") Long pageNumber,
                                                      @Query("pageSize") Integer pageSize);

    @GET("/api/accounts/token")
    Call<BaseResponse<TokenResponse>> getToken(@Query("deviceId") String query);

    @GET("/api/sale/search")
    Call<BaseResponse<PurchaseDetailsResponse>> track(@Query("referenceNumber") String number);

    @POST("/api/sale/feedback")
    Call<BaseResponse<Object>> postFeedback(@Body FeedbackModel feedbackModel);

    @GET("/api/country")
    Call<BaseResponse<List<Country>>> getCountries();

    @GET("/api/charge-matrix/importcharge")
    Call<BaseResponse<Charge>> getCharge(@Query("model.artpieceId") String artId, @Query("model.destinationCountryId") String countryId);

    @POST("/api/sale")
    Call<BaseResponse<String>> sendSale(@Body Sale sale);

    @POST("/api/artpiece/{id}/reserve")
    Call<BaseResponse<Void>> reserveArt(@Path("id") long id);

    @POST("/api/artpiece/{id}/release")
    Call<BaseResponse<Void>> releaseArt(@Path("id") long id);

    @POST("/api/artpiece/beam-marker")
    Call<BaseResponse<Void>> sendMarkerByEmail(@Body MarkerModel model);

}
