package com.vuforia.vuforia.dagger;

import com.vuforia.vuforia.VuforiaApplication;
import com.vuforia.vuforia.presentation.ui.SearchActivity;
import com.vuforia.vuforia.presentation.ui.SendBeamerActivity;
import com.vuforia.vuforia.presentation.ui.SplashActivity;
import com.vuforia.vuforia.presentation.ui.TrackPurchaseActivity;
import com.vuforia.vuforia.presentation.ui.beam.BeamActivity;
import com.vuforia.vuforia.presentation.ui.country.CountryActivity;
import com.vuforia.vuforia.presentation.ui.paypal.BuyActivity;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {DataModule.class, AndroidSupportInjectionModule.class})
public interface VufComponent {

    void inject(VuforiaApplication vufApplication);

    void inject(SearchActivity activity);

    void inject(BeamActivity instance);

    void inject(TrackPurchaseActivity instance);

    void inject(CountryActivity countryActivity);

    void inject(BuyActivity buyActivity);

    void inject(SplashActivity splashActivity);

    void inject(SendBeamerActivity beamerActivity);
}
