package com.vuforia.vuforia.presentation.ui.country;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.model.responce.Country;

import java.util.ArrayList;
import java.util.List;

class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    private final List<Country> data = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country, parent, false));
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        final Country country = data.get(position);
        holder.country.setText(country.getName());
        holder.parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClicked(country);
                }
            }
        });
    }

    void updateData(List<Country> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    interface ItemClickListener {
        void onItemClicked(Country country);
    }

    static class CountryViewHolder extends RecyclerView.ViewHolder {

        final View parent;
        TextView country;

        CountryViewHolder(View parent) {
            super(parent);
            this.parent = parent;
            country = (TextView) parent.findViewById(R.id.ic_country);
        }
    }
}
