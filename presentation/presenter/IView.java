package com.vuforia.vuforia.presentation.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.vuforia.vuforia.data.CallServerException;

public interface IView {

    @NonNull
    Context getContext();

    void showProgress();

    void hideProgress();

    void showError(@StringRes int error);

    void showError(String error);

    void showError(CallServerException ex);
}
