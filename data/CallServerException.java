package com.vuforia.vuforia.data;

import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.text.TextUtils;

public class CallServerException extends Exception {

    private
    @StringRes
    int userFriendlyErrorTextResourses = -1;

    private String userFriendlyErrorText;

    public CallServerException(String error, @StringRes int userFriendlyErrorText) {
        super(error);
        this.userFriendlyErrorTextResourses = userFriendlyErrorText;
    }

    public CallServerException(String error, String userFriendlyErrorText) {
        super(error);
        this.userFriendlyErrorText = userFriendlyErrorText;
    }

    public String getUserFriendlyErrorText(Resources resources) {
        if (userFriendlyErrorTextResourses == -1) {
            if (TextUtils.isEmpty(userFriendlyErrorText)) {
                throw new IllegalArgumentException("Must be at least 1 error decsription");
            } else {
                return userFriendlyErrorText;
            }
        } else {
            return resources.getString(userFriendlyErrorTextResourses);
        }
    }
}
