package com.vuforia.vuforia.presentation.ui.beam;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

abstract class MeshObject {

    Buffer getVertices() {
        return getBuffer(BufferType.BUFFER_TYPE_VERTEX);
    }

    Buffer getTexCoords() {
        return getBuffer(BufferType.BUFFER_TYPE_TEXTURE_COORD);
    }

    Buffer getIndices() {
        return getBuffer(BufferType.BUFFER_TYPE_INDICES);
    }

    Buffer fillBuffer(float... array) {
        ByteBuffer bb = ByteBuffer.allocateDirect(4 * array.length);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (float d : array) {
            bb.putFloat(d);
        }
        bb.rewind();

        return bb;
    }

    Buffer fillBuffer(short... array) {
        // Each short takes 2 bytes
        ByteBuffer bb = ByteBuffer.allocateDirect(2 * array.length);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (short s : array) {
            bb.putShort(s);
        }
        bb.rewind();

        return bb;
    }

    public abstract Buffer getBuffer(BufferType bufferType);

    public abstract int getNumObjectIndex();

    enum BufferType {
        BUFFER_TYPE_VERTEX, BUFFER_TYPE_TEXTURE_COORD, BUFFER_TYPE_NORMALS, BUFFER_TYPE_INDICES
    }
}
