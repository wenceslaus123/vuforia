package com.vuforia.vuforia.domain.repository;

import com.vuforia.vuforia.data.model.responce.ArtpieceSearchResponse;

import io.reactivex.Observable;


public interface PicturesRepository {
    int PAGE_SIZE = 10;

    Observable<ArtpieceSearchResponse> search(String searchQuery, long page);
}
