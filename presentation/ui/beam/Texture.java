package com.vuforia.vuforia.presentation.ui.beam;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import timber.log.Timber;


// Support class for the Vuforia samples applications.
// Exposes functionality for loading a texture from the APK.
class Texture {

    int mWidth;          // The width of the texture.
    int mHeight;         // The height of the texture.
    ByteBuffer mData;    // The pixel data.
    int[] mTextureID = new int[1];
    private int mChannels;       // The number of channels.


    static Texture loadTextureFromAassets(String fileName, AssetManager assets) {
        InputStream inputStream = null;
        try {
            inputStream = assets.open(fileName, AssetManager.ACCESS_BUFFER);

            BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);
            Bitmap bitMap = BitmapFactory.decodeStream(bufferedStream);

            return convertBitmapToTexture(bitMap);
        } catch (IOException e) {
            Timber.e("Failed to Timber texture '%s' from APK", fileName);
            Timber.i(e.getMessage());
            return null;
        }
    }

    static Texture convertBitmapToTexture(Bitmap bitMap) {
        int[] data = new int[bitMap.getWidth() * bitMap.getHeight()];
        bitMap.getPixels(data, 0, bitMap.getWidth(), 0, 0,
                bitMap.getWidth(), bitMap.getHeight());

        return loadTextureFromIntBuffer(data, bitMap.getWidth(),
                bitMap.getHeight());
    }


    private static Texture loadTextureFromIntBuffer(int[] data, int width, int height) {
        // Convert:
        int numPixels = width * height;
        byte[] dataBytes = new byte[numPixels * 4];

        for (int p = 0; p < numPixels; ++p) {
            int colour = data[p];
            dataBytes[p * 4] = (byte) (colour >>> 16); // R
            dataBytes[p * 4 + 1] = (byte) (colour >>> 8); // G
            dataBytes[p * 4 + 2] = (byte) colour; // B
            dataBytes[p * 4 + 3] = (byte) (colour >>> 24); // A
        }

        Texture texture = new Texture();
        texture.mWidth = width;
        texture.mHeight = height;
        texture.mChannels = 4;

        texture.mData = ByteBuffer.allocateDirect(dataBytes.length).order(
                ByteOrder.nativeOrder());
        int rowSize = texture.mWidth * texture.mChannels;
        for (int r = 0; r < texture.mHeight; r++) {
            texture.mData.put(dataBytes, rowSize * (texture.mHeight - 1 - r), rowSize);
        }

        texture.mData.rewind();

        return texture;
    }
}
