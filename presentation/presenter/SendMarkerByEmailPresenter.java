package com.vuforia.vuforia.presentation.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.vuforia.vuforia.R;
import com.vuforia.vuforia.data.CallServerException;
import com.vuforia.vuforia.domain.model.MarkerModel;
import com.vuforia.vuforia.domain.usecase.SendMarkerCase;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

import static com.vuforia.vuforia.utils.Validator.isEmailValid;

public class SendMarkerByEmailPresenter extends Presenter<SendMarkerByEmailPresenter.SendMarkerView> {
    private final SendMarkerCase sendMarkerCase;

    @Inject
    public SendMarkerByEmailPresenter(SendMarkerCase sendMarkerCase) {
        this.sendMarkerCase = sendMarkerCase;
    }

    private void send(MarkerModel model) {
        getView().showProgress();
        sendMarkerCase.execute(new SendMarkerByEmailPresenter.SendMarkerResultObserver(), model);
    }


    public void sendMarkerByEmail(String email) {
        if (isEmailValid(email)) {
            MarkerModel markerModel = MarkerModel.builder()
                    .email(email)
                    .build();
            send(markerModel);
        } else {
            getView().showErrorEmail(R.string.error_email);
        }
    }

    public interface SendMarkerView extends IView {
        void showResult();

        void showError();

        void showErrorEmail(@StringRes int id);
    }


    private class SendMarkerResultObserver extends DisposableObserver<Void> {


        @Override
        public void onNext(@NonNull Void aVoid) {
            // Nothing here
        }

        @Override
        public void onError(@NonNull Throwable e) {
            SendMarkerByEmailPresenter.SendMarkerView view = getView();
            if (view != null) {
                view.hideProgress();
                if (e instanceof CallServerException) {
                    view.showError(((CallServerException) e).getUserFriendlyErrorText(view.getContext().getResources()));
                }
            }
        }

        @Override
        public void onComplete() {
            SendMarkerByEmailPresenter.SendMarkerView view = getView();
            if (view != null) {
                view.hideProgress();
                view.showResult();
            }
        }
    }
}
