package com.vuforia.vuforia.presentation.presenter;

import android.support.annotation.NonNull;

public interface IPresenter<V extends IView> {

    void attachView(@NonNull V view);

    void detachView();
}
