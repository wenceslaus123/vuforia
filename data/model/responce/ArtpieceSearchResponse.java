package com.vuforia.vuforia.data.model.responce;

import com.vuforia.vuforia.domain.model.ArtPiece;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ArtpieceSearchResponse {
    @SerializedName("items")
    @Expose
    public List<ArtpieceItem> items = null;

    @SerializedName("totalCount")
    @Expose
    public long totalCount;

    @SerializedName("totalPages")
    @Expose
    public long totalPages;

    @SerializedName("page")
    @Expose
    public long page;

    @SerializedName("pageSize")
    @Expose
    public long pageSize;

    public List<ArtPiece> convertToArtpiceList() {
        ArrayList<ArtPiece> result = new ArrayList<>();
        for (ArtpieceItem item : items) {
            result.add(ArtPiece.builder()
                    .id(item.id)
                    .name(item.name)
                    .height(item.height)
                    .width(item.width)
                    .beamHeight((int) item.beamHeight)
                    .beamWidth((int) item.beamWidth)
                    .measuringType(item.measuringType)
                    .placeName(item.placeName)
                    .price(item.price)
                    .mediumType(item.mediumType)
                    .isFramed(item.isFramed)
                    .suitableRooms(item.suitableRooms)
                    .imageUrl(item.imageUrl)
                    .thumbImageUrl(item.thumbImageUrl)
                    .artistName(item.artistName)
                    .bio(item.bio)
                    .artistPhoto(item.avatarUrl)
                    .artistCountry(item.artistCountry)
                    .sizeIndicator(item.sizeIndicator)
                    .currency(item.currency)
                    .build());
        }
        return result;
    }

}
