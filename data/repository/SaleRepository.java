package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.domain.model.Sale;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SaleRepository {
    private final Api api;

    @Inject
    public SaleRepository(Api api) {
        this.api = api;
    }

    public Observable<String> sendSale(Sale sale) {
        return Observable.create(new NetworkRequestExecutor<>(api.sendSale(sale)));
    }

    public Observable<Void> reserveArt(long id) {
        return Observable.create(new NetworkRequestExecutor<>(api.reserveArt(id)));
    }

    public Observable<Void> releaseeArt(long id) {
        return Observable.create(new NetworkRequestExecutor<>(api.releaseArt(id)));
    }
}
