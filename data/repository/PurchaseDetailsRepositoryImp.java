package com.vuforia.vuforia.data.repository;

import com.vuforia.vuforia.data.Api;
import com.vuforia.vuforia.data.model.responce.PurchaseDetailsResponse;
import com.vuforia.vuforia.domain.model.PurchaseDetailModel;
import com.vuforia.vuforia.domain.repository.PurchaseDetailsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class PurchaseDetailsRepositoryImp implements PurchaseDetailsRepository {
    private final Api api;

    @Inject
    public PurchaseDetailsRepositoryImp(Api api) {
        this.api = api;
    }

    @Override
    public Observable<PurchaseDetailModel> track(String searchQuery) {
        return Observable.create(new NetworkRequestExecutor<>(api.track(searchQuery)))
                .map(new Function<PurchaseDetailsResponse, PurchaseDetailModel>() {
                    @Override
                    public PurchaseDetailModel apply(@NonNull PurchaseDetailsResponse purchaseTrackResponse) throws Exception {
                        return purchaseTrackResponse.convertToPurchaseModel();
                    }
                });


    }
}
